<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTaste extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_tastes';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['product_id', 'code', 'distributor_code', 'name', 'outlet_type_id', 'pcs'];

	/*
	Relationships
	*/
	public function product()
	{
		return $this->belongsTo('App\Product');
	}

	public function outletType()
	{
		return $this->belongsTo('App\OutletType');
	}

	public function stock()
	{
		return $this->hasMany('App\Stock');
	}


	/*
	Custom function
	*/
	public static function rowExist($field, $param)
	{
		$product_taste = ProductTaste::where($field, '=', $param)->first();

		if(!empty($product_taste))
		{
			return $product_taste->id;
		}
		else
		{
			return FALSE;
		}
	}

	public static function outletTypeExist($outlet_type_id, $field, $param)
	{
		$product_taste = ProductTaste::where('outlet_type_id', '=', $outlet_type_id)->where($field, '=', $param)->first();

		if(!empty($product_taste))
		{
			return array('id' => $product_taste->id, 'pcs' => $product_taste->pcs);
		}
		else
		{
			return FALSE;
		}

	}

}
