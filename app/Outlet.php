<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Outlet extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'outlets';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['code', 'name', 'address', 'type', 'area', 'data_file_id', 'outlet_type_id', 'outlet_type_detail_id', 'outlet_group_id'];

	public function outletType()
	{
		return $this->belongsTo('App\OutletType');
	}

	/* Custom function */

	public static function rowExist($field, $identifier)
	{
		$outlet = Outlet::where($field, '=', $identifier)->first();

		if(!empty($outlet))
		{
			return $outlet->id;
		}
		else
		{
			return FALSE;
		}
	}

	public static function ChecknInsert($field, $param)
	{
		$outlet = Outlet::rowExist($field, $param);

		if($outlet == FALSE)
		{
			$outlet_insert = Outlet::create([$field => $param]);

			return $outlet_insert->id;
		}
		else
		{
			return $outlet;
		}
	}

}
