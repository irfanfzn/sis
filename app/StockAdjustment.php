<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class StockAdjustment extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'stock_adjustments';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
			'stock_id',
			'exp_date',
			'up2',
			'up3'
		];

	public function Stock()
	{
		$this->belongsTo('App\Stock');
	}

	/*Custom function*/

	public static function rowExist($exp_date, $product_taste_id, $depo_id)
	{
		$stock = StockAdjustment::join('stocks', 'stocks.id', '=', 'stock_adjustments.stock_id')
				->where('exp_date', '=', $exp_date)
				->where('product_taste_id', '=', $product_taste_id)
				->where('depo_id', '=', $depo_id)
				->first();

		if(!empty($stock))
		{
			return $stock->id;
		}
		else
		{
			return "FALSE";
		}
	}

	public static function getSumStockUp($stock_id)
	{
		$stock = StockAdjustment::select(\DB::raw('(sum(up2) + sum(up3)) as sum_up'))
								->where('stock_id', '=', $stock_id)
								->first();
		if(!empty($stock))
		{
			return $stock->sum_up;
		}
		else
		{
			return "FALSE";
		}
	}

	public static function getTotalAdj($stock_id)
	{
		$stock_adj = StockAdjustment::select(\DB::raw('sum(up2) as up2, sum(up3) as up3'))
					->where('stock_id', '=', $stock_id)
					->groupBy('stock_id')
					->first();

		if(empty($stock_adj))
		{
			return ['up2' => 0, 'up3' => 0];
		}
		else
		{
			return ['up2' => $stock_adj->up2, 'up3' => $stock_adj->up3];
		}
	}
}
