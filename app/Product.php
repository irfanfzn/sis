<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['code', 'name', 'brand_id'];

	/*
	Relationships
	*/

	public function brand()
	{
		return $this->belongsTo('App\Brand');
	}

	public function producTastes()
	{
		return $this->hasMany('App\ProductTaste');
	}

	/*
	Custom function
	*/

	public static function rowExist($field, $param)
	{
		if($param == "")
			return FALSE;

		$product = Product::where($field, '=', $param)->first();

		if(!empty($product))
		{
			return $product->id;
		}
		else
		{
			return FALSE;
		}

	}

	/*public static function ChecknInsert($field, $param)
	{
		$product = Product::rowExist($field, $param);

		if($product == FALSE)
		{
			$product_insert = Product::create([$field => $param]);

			return $product_insert->id;
		}
		else
		{
			return $product;
		}
	}*/
}
