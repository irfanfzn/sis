<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BestEstimateTarget extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'best_estimate_targets';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['product_id', 'area_id', 'outlet_type_id', 'outlet_group_id', 'target_month', 'month_num', 'target_year', 'target'];

	/*
	Custom function
	*/

	public static function checkDouble($params = "")
	{
		$where_content = "";
		foreach($params as $field => $param)
		{
			$where_content .= "$field = $param and ";
		}
		$where_content = substr($where_content, 0, -5);

		$be_target = BestEstimateTarget::whereRaw($where_content)
							->first();

		if(empty($be_target))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public static function filter($actual_target, $filters = "")
	{
		if(!empty($filters['year']))
		{
			$actual_target = $actual_target->where('target_year', '=', $filters['year']);
		}
		else
		{
			$actual_target = $actual_target->where('target_year', '=', Date('Y'));
		}

		if(!empty($filters['area_ids']))
		{
			$actual_target = $actual_target->whereIn('areas.id', $filters['area_ids']);
		}

		if(!empty($filters['outlet_type_ids']))
		{
			$actual_target = $actual_target->whereIn('outlet_types.id', $filters['outlet_type_ids']);
		}

		if(!empty($filters['outlet_group_ids']))
		{
			$actual_target = $actual_target->whereIn('outlet_groups.id', $filters['outlet_group_ids']);
		}

		if(!empty($filters['product_ids']))
		{
			$actual_target = $actual_target->whereIn('products.id', $filters['product_ids']);
		}

		return $actual_target;
	}

	public static function chartTarget($filters = "")
	{
		$target = BestEstimateTarget::select(\DB::raw('sum(target) as target, target_month, month_num'))
					->join('areas', 'areas.id', '=', 'best_estimate_targets.area_id')
					->join('outlet_types', 'outlet_types.id', '=', 'best_estimate_targets.outlet_type_id')
					->join('outlet_groups', 'outlet_groups.id', '=', 'best_estimate_targets.outlet_group_id')
					->join('products', 'products.id', '=', 'best_estimate_targets.product_id');
		
		$target = Self::filter($target, $filters);
		
		$target = $target->groupBy('target_month')
					->orderBy('month_num')
					->get();


		$arr_chart = array();

		if(!empty($target))
		{
			$index = 0;
			$index_be = array();
			foreach($target as $row)
			{
				$index_be[$row->month_num]['month'] = substr($row->target_month, 0, 3);
				$index_be[$row->month_num]['be'] = (integer)$row->target;

				$index++;
			}

			for($a=1;$a<=12;$a++)
			{
				$arr_chart[$a]['label'] = !empty($index_be[$a]) ? $index_be[$a]['month'] : 0;
				$arr_chart[$a]['y'] = !empty($index_be[$a]) ? $index_be[$a]['be'] : 0;
			}	
		}

		return $arr_chart;
	}

}
