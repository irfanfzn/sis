<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Distributor extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'distributors';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	/*
	Custom function
	*/
	
	public static function rowExist($field, $param)
	{
		$branch = Distributor::where($field, '=', $param)->first();

		if(!empty($branch))
		{
			return $branch->id;
		}
		else
		{
			return FALSE;
		}
	}

	public static function ChecknInsert($field, $param)
	{
		if($param == "")
			return FALSE;
		
		$distributor = Distributor::rowExist($field, $param);

		if($distributor == FALSE)
		{
			$distributor_insert = Distributor::create([$field => $param]);

			return $distributor_insert->id;
		}
		else
		{
			return $distributor;
		}
	}

}
