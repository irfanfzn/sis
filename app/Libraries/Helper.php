<?php namespace App\Libraries;

use Illuminate\Support\Str;
use Helfull\CanvasJS\Chart;
use Helfull\CanvasJS\Chart\ChartData;
use Helfull\CanvasJS\Chart\DataPoint;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Outlet;
use App\Stock;
use App\Product;
use App\Taste;
use App\ProductTaste;
use Input;

class Helper {

	public static function OutletData()
	{
	    $outlet = Outlet::all();
	    return $outlet;
	}

	public static function StockData()
	{
		$stocks = Stock::all();

		return $stocks;		
	}

	// public static function getSearchParam($param, $field)
	// {
	// 	$get_data = $field::select(\DB::raw("distinct $param as content"))->get();

	// 	$data = response()->json($get_data);

	// 	return $data;
	// }

	public static function ShowMessage($message = null, $type = null)
	{
		if($type = 1)
		{
			return '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">×</button> <strong>Success!</strong> '. $message .'</div>';
		}
		else
		{
			return '<div class="alert alert-failed"> <button type="button" class="close" data-dismiss="alert">×</button> <strong>Failed!</strong> '. $message .'</div>';
		}
	}

	public static function ErrorMessage($errors)
	{
		if (count($errors) > 0)
		{
			$message = '<div class="alert alert-danger">';
			$message .=	'<strong>Whoops!</strong> There were some problems with your input.<br><br>';
			$message .= '<ul>';
			
			foreach ($errors->all() as $error)
			{
				$message .= "<li>$error</li>";
			}
			
			$message .=	'</ul>';
			$message .= '</div>';

			return $message;
		}
	}

	public static function EmptyWarning($column_name = null, $row = null)
	{
		return '<b>'.$column_name.'</b> is empty in row <b>'.$row.'</b>';
	}

	public static function CheckFormat($fields = array(), $checked_array = array())
	{	
		$index = 0;
		
		foreach($fields as $key => $field)
		{	
			
			if(!empty($key))
			{				
				if (!in_array($key, array_map('strtolower', $checked_array))){
	    			return FALSE;
				}
			}
			/*else
			{
				return FALSE;
			}*/

			$index++;
		}

		return TRUE;
	}

	public static function GenerateCode($id_product, $id_taste, $outlet_type)
	{
		$prefix = 'W';

		/* Get product code */
		$product = Product::where('id', '=', $id_product)->first()->code;

		/* Get Taste code */
		$taste = Taste::where('id', '=', $id_taste)->first()->code;

		/* Get max id of product_taste */
		$product_taste = ProductTaste::max('id');

		if(empty($product_taste))
		{
			$number = 0;
		}
		else
		{
			$number = $product_taste;
		}

		$number = $number + 1;

		$number = str_pad($number, 3, '0', STR_PAD_LEFT);

		// $area = 'LPJ'; Untuk Luar Pulau Jawa

		// $area = 'PJ'; // Untuk Pulau Jawa

		$generated_code = $prefix.$product.$taste.$number.$outlet_type;

		return $generated_code;
	}

	public static function Currency($number)
	{
		$curr = "Rp.";

		$curr .= number_format($number);

		return $curr;
	}

	public static function GenerateCtn($qty, $formula)
	{
		$value = $qty / $formula;

		return $value;
	}

	public static function CheckSpecialProduct($product)
	{
		$find_gizo = strpos($product, 'GIZO');

		if($find_gizo !== false)
		{
			$space_count = substr_count($product, ' ');
			$arr_product_taste = explode(' ', $product, 2);
			$product_taste_name = $arr_product_taste[1];

			$find_tama = strpos($product, 'TAMA');
			if($find_tama !== false && $space_count <= 1)
			{
				$product_taste_name	= $arr_product_taste[1].' MILK';
			}

			$find_wow = strpos($product, 'WOW');
			if($find_wow !== false && $space_count <= 1)
			{
				$product_taste_name	= $arr_product_taste[1].' STRAWBERRY';
			}

			return $product_taste_name;
		}
		else
		{
			return FALSE;
		}
	}

	public static function StringFormat($value)
	{
		$value = "'$value'";

		return $value;
	}

	public static function MonthFilter($value = 0)
	{
		$month_name = [
			1 => 'January',
			2 => 'February',
			3 => 'March',
			4 => 'April',
			5 => 'May',
			6 => 'June',
			7 => 'July',
			8 => 'August',
			9 => 'September',
			10 => 'October',
			11 => 'November',
			12 => 'December'
		];
		
		if($value != 0)
		{
			$months = array_combine(range(1, count($value)), array_values($value));

			foreach($month_name as $key => $name)
			{
				if(!array_search($key, $months))
				{
					unset($month_name[$key]);
				}
			}
		}
		
		// $month_lists = array_values($month_name);

		return $month_name;
	}

	public static function MonthNumber($value)
	{
		$month_name = [
			1 => 'January',
			2 => 'February',
			3 => 'March',
			4 => 'April',
			5 => 'May',
			6 => 'June',
			7 => 'July',
			8 => 'August',
			9 => 'September',
			10 => 'October',
			11 => 'November',
			12 => 'December'
		];
		
		$number = array_search($value, $month_name);

		return $number;
	}

	public static function GetInput($input, $value)
	{
		$selected = "";
		if(!empty($input))
		{
			if(is_array($input))
			{
				if(in_array($value, $input))
				{
					$selected = 'selected';
				}	
			}
			else
			{
				if($input == $value)
					$selected = 'selected';
			}
		}

		return $selected;
	}

	public static function GenerateChart($data, $title = "")
	{
		$chart_data = [
			"chart" => [
		        "theme"=> "theme3",
		        
	        	"title"=>["text" => $title],

		        "animationEnabled"=> true,   // change to true

		        "axisX" => [
		        	"interval" => 1,
		        	"labelFontSize" => 14
		        ],

		        "axisY" => [
		        	"labelFontSize" => 14
		        ],

		        "data"=> $data
		        			
	    	]
    	];

		$chart = new Chart($chart_data);

		return $chart;
	}

	public static function TrendChartData($trend, $chart_type, $year)
	{
		$year_diff = $year - 1;
		$chart_data = [
        	[// Change type to "bar", "splineArea", "area", "spline", "pie",etc.
	        	"type" => $chart_type,
	        	"showInLegend" => true,
	        	"legendText" => "Actual target $year",
	            "dataPoints"=> $trend['actual_target']
	            
            ],
            [
            	"type" => $chart_type,
            	"showInLegend" => true,
	        	"legendText" => "Best estimate $year",
            	"dataPoints" => $trend['best_estimate']
            ],
            [
            	"type" => $chart_type,
            	"showInLegend" => true,
	        	"legendText" => "Sell Out $year",
            	"dataPoints" => $trend['sellout_data']
            ],
            [
            	"type" => $chart_type,
            	"showInLegend" => true,
	        	"legendText" => "Sell Out $year_diff",
            	"dataPoints" => $trend['sellout_data_b']
            ]

    	];

    	return $chart_data;
	}

	public static function getUserRole()
	{
		$user = \Auth::user()->UserRole->name;

		return $user;
		// $user_role = App\UserRole::where('user_id')
	}

	public static function getAreaHandler()
	{
		$user = \Auth::user()->UserArea()->first()->area_id;
		
		return $user;
	}

	public static function DateFormat($val)
	{
		$date = Date('M, d Y', strtotime($val));

		return $date;
	}
}

?>