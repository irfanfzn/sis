<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ActualTarget extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'actual_targets';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['product_id', 'area_id', 'outlet_type_id', 'outlet_group_id', 'target_month', 'month_num', 'target_year', 'target'];

	public function Area()
	{
		return $this->belongsTo('App\Area');
	}

	/*
	Custom function
	*/

	public static function checkDouble($params = "")
	{
		$where_content = "";
		foreach($params as $field => $param)
		{
			$where_content .= "$field = $param and ";
		}
		$where_content = substr($where_content, 0, -5);

		$actual_target = ActualTarget::whereRaw($where_content)
							->first();

		if(empty($actual_target))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public static function getFiltered($month_counts = 0, $filters = "")
	{
		// dd($filters);
		$month = "";
		if($month_counts == 0)
		{
			for($a=1;$a<=12;$a++)
			{
				$month .= "sum(if(month_num = $a, actual_targets.target, 0)) as target_$a,"; 
			}	
		}
		else
		{
			foreach($month_counts as $a)
			{
				$month .= "sum(if(month_num = $a, actual_targets.target, 0)) as target_$a,";
			}
		}

		$month = substr($month, 0, -1);

		$actual_target = ActualTarget::select(\DB::raw("
				areas.name as area_name, 
				$month"
				))
			->join('areas', 'areas.id', '=', 'actual_targets.area_id')
			->join('outlet_types', 'outlet_types.id', '=', 'actual_targets.outlet_type_id')
			->join('outlet_groups', 'outlet_groups.id', '=', 'actual_targets.outlet_group_id')
			->join('products', 'products.id', '=', 'actual_targets.product_id');

		$actual_target = Self::filter($actual_target, $filters);

		$actual_target = $actual_target->groupBy('areas.name')
			->get()
			->toArray();

		return $actual_target;
	}

	public static function filter($actual_target, $filters = "")
	{
		if(!empty($filters['year']))
		{
			$actual_target = $actual_target->where('target_year', '=', $filters['year']);
		}
		else
		{
			$actual_target = $actual_target->where('target_year', '=', Date('Y'));
		}

		if(!empty($filters['area_ids']))
		{
			$actual_target = $actual_target->whereIn('areas.id', $filters['area_ids']);
		}

		if(!empty($filters['outlet_type_ids']))
		{
			$actual_target = $actual_target->whereIn('outlet_types.id', $filters['outlet_type_ids']);
		}

		if(!empty($filters['outlet_group_ids']))
		{
			$actual_target = $actual_target->whereIn('outlet_groups.id', $filters['outlet_group_ids']);
		}

		if(!empty($filters['product_ids']))
		{
			$actual_target = $actual_target->whereIn('products.id', $filters['product_ids']);
		}

		return $actual_target;
	}

	public static function chartTarget($filters = "")
	{
		$target = ActualTarget::select(\DB::raw('sum(target) as target, target_month'))
					->join('areas', 'areas.id', '=', 'actual_targets.area_id')
					->join('outlet_types', 'outlet_types.id', '=', 'actual_targets.outlet_type_id')
					->join('outlet_groups', 'outlet_groups.id', '=', 'actual_targets.outlet_group_id')
					->join('products', 'products.id', '=', 'actual_targets.product_id');
					
		$target = Self::filter($target, $filters);
		
		$target = $target->groupBy('target_month')
					->orderBy('month_num')
					->get();
		// dd($target);
		$arr_chart = array();

		if(!empty($target))
		{
			$index = 0;
			foreach($target as $row)
			{
				$arr_chart[$index]['label'] = substr($row->target_month, 0, 3);
				$arr_chart[$index]['y'] = (integer)$row->target;

				$index++;
			}	
		}
		
		return $arr_chart;
	}
}
