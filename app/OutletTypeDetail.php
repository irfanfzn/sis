<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class OutletTypeDetail extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'outlet_type_details';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['name'];

	public static function rowExist($field, $param)
	{
		$outlet_type_detail = OutletTypeDetail::where($field, '=', $param)->first();

		if(!empty($outlet_type_detail))
		{
			return $outlet_type_detail->id;
		}
		else
		{
			return FALSE;
		}
	}

	public static function ChecknInsert($field, $param)
	{
		$outlet_type_detail = OutletTypeDetail::rowExist($field, $param);

		if($outlet_type_detail == FALSE)
		{
			$outlet_type_detail_insert = OutletTypeDetail::create([$field => $param]);

			return $outlet_type_detail_insert->id;
		}
		else
		{
			return $outlet_type_detail;
		}
	}

}
