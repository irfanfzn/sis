<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SellOut extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sell_outs';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
						'product_taste_id',
						'outlet_type_id',	
						'depo_id',
						'sale_id', 
						'sales_promotion_id', 
						'area_id',
						'sell_date',
						'qty_ctn',
						'qty_pcs',
						'qty_converted_pcs',
						'qty_total',
						'return_ctn',
						'return_pcs',
						'return_converted_pcs',
						'return_total',
						'omzet',
						'net',
						'distributor_id',
						'outlet_id'
					];

	/*
	Custom function
	*/

	public static function rowExist($field, $param)
	{
		$sell_out = SellOut::where($field, '=', $param)->first();

		if(!empty($sell_out))
		{
			return $sell_out->id;
		}
		else
		{
			return FALSE;
		}
	}

	public static function checkDouble($params = "")
	{
		$where_content = "";
		foreach($params as $field => $param)
		{
			$where_content .= "$field = $param and ";
		}
		$where_content = substr($where_content, 0, -5);

		$sell_out = SellOut::whereRaw($where_content)
							->first();

		if(empty($sell_out))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public static function getSelloutFiltered($report_by = "areas", $month_counts = 0, $filters = "")
	{
		// dd($filters);
		$month = "";
		if($month_counts == 0)
		{
			for($a=1;$a<=12;$a++)
			{
				$month .= "sum(if(month(sell_outs.sell_date) = $a, sell_outs.qty_total, 0)) as ctn_$a, 
							sum(if(month(sell_outs.sell_date) = $a, sell_outs.return_total, 0)) as return_$a,
							sum(if(month(sell_outs.sell_date) = $a, sell_outs.omzet, 0)) as omzet_$a,
							sum(if(month(sell_outs.sell_date) = $a, sell_outs.net, 0)) as net_$a,";
			}	
		}
		else
		{
			foreach($month_counts as $a)
			{
				$month .= "sum(if(month(sell_outs.sell_date) = $a, sell_outs.qty_total, 0)) as ctn_$a, 
							sum(if(month(sell_outs.sell_date) = $a, sell_outs.return_total, 0)) as return_$a,
							sum(if(month(sell_outs.sell_date) = $a, sell_outs.omzet, 0)) as omzet_$a,
							sum(if(month(sell_outs.sell_date) = $a, sell_outs.net, 0)) as net_$a,";
			}
		}

		$month = substr($month, 0, -1);

		$sellout = Sellout::select(\DB::raw("
				$report_by.name as area_name, 
				$month"
				))
			->join('areas', 'areas.id', '=', 'sell_outs.area_id')
			->join('regions', 'regions.id', '=', 'areas.region_id')
			->join('parent_regions', 'parent_regions.id', '=', 'regions.parent_region_id')
			->join('depos', 'depos.id', '=', 'sell_outs.depo_id')
			->join('distributors', 'distributors.id', '=', 'sell_outs.distributor_id')
			->join('outlet_types', 'outlet_types.id', '=', 'sell_outs.outlet_type_id')
			->join('outlets', 'outlets.id', '=', 'sell_outs.outlet_id')
			->join('outlet_type_details', 'outlet_type_details.id', '=', 'outlets.outlet_type_detail_id')
			->join('outlet_groups', 'outlet_groups.id', '=', 'outlets.outlet_group_id')
			->join('product_tastes', 'product_tastes.id', '=', 'sell_outs.product_taste_id')
			->join('products', 'products.id', '=', 'product_tastes.product_id');

		$sellout = Self::filter($sellout, $filters);

		$sellout = $sellout->groupBy("$report_by.id")
			->get()
			->toArray();

		return $sellout;
	}

	public static function filter($sellout, $filters)
	{
		if(!empty($filters['year']))
		{
			$sellout = $sellout->whereRaw('year(sell_date) = '. $filters['year']);
		}
		else
		{
			$sellout = $sellout->whereRaw('year(sell_date) = 2015');
		}
		
		if(!empty($filters['parent_region_ids']))
		{
			$sellout = $sellout->whereIn('parent_regions.id', $filters['parent_region_ids']);
		}

		if(!empty($filters['region_ids']))
		{
			$sellout = $sellout->whereIn('regions.id', $filters['region_ids']);
		}

		if(!empty($filters['depo_ids']))
		{
			$sellout = $sellout->whereIn('depos.id', $filters['depo_ids']);
		}	

		if(!empty($filters['area_ids']))
		{
			$sellout = $sellout->whereIn('areas.id', $filters['area_ids']);
		}

		if(!empty($filters['outlet_type_ids']))
		{
			$sellout = $sellout->whereIn('outlet_types.id', $filters['outlet_type_ids']);
		}

		if(!empty($filters['distributor_ids']))
		{
			$sellout = $sellout->whereIn('distributors.id', $filters['distributor_ids']);
		}

		if(!empty($filters['product_ids']))
		{
			$sellout = $sellout->whereIn('products.id', $filters['product_ids']);
		}

		if(!empty($filters['outlet_type_detail_ids']))
		{
			$sellout = $sellout->whereIn('outlet_type_details.id', $filters['outlet_type_detail_ids']);
		}

		if(!empty($filters['outlet_group_ids']))
		{
			$sellout = $sellout->whereIn('outlet_groups.id', $filters['outlet_group_ids']);
		}

		if(!empty($filters['sale_ids']))
		{
			$sellout = $sellout->whereIn('sale.id', $filters['sale_ids']);
		}

		if(!empty($filters['product_taste_ids']))
		{
			$filters = $filters['product_taste_ids'];
			$sellout = $sellout->where(function($query) use ($filters)
			{
				foreach($filters as $key => $data)
				{
					if($key == 0)
					{
						$query->Where('product_tastes.name', '=', $data);	
					}
					else
					{
						$query->orWhere('product_tastes.name', '=', $data);
					}
					
				}	
			});
		}

		return $sellout;
	}

	public static function getYear()
	{
		$year = Sellout::select(\DB::raw('YEAR(sell_date) as sell_year'))
				->groupBy(\DB::raw('YEAR(sell_date)'))
				->get();

		return $year;
	}

	public static function chartTarget($filters = "")
	{
		$sellout = Sellout::select(\DB::raw("sum(net) as net, monthname(STR_TO_DATE(month(sell_date), '%m')) as sell_month, month(sell_date) as month_num"))
					->join('areas', 'areas.id', '=', 'sell_outs.area_id')
					->join('regions', 'regions.id', '=', 'areas.region_id')
					->join('parent_regions', 'parent_regions.id', '=', 'regions.parent_region_id')
					->join('depos', 'depos.id', '=', 'sell_outs.depo_id')
					->join('outlet_types', 'outlet_types.id', '=', 'sell_outs.outlet_type_id')
					->join('outlets', 'outlets.id', '=', 'sell_outs.outlet_id')
					->join('outlet_type_details', 'outlet_type_details.id', '=', 'outlets.outlet_type_detail_id')
					->join('outlet_groups', 'outlet_groups.id', '=', 'outlets.outlet_group_id')
					->join('product_tastes', 'product_tastes.id', '=', 'sell_outs.product_taste_id')
					->join('products', 'products.id', '=', 'product_tastes.product_id');

		$sellout = Self::filter($sellout, $filters);

		$sellout = $sellout->groupBy(\DB::raw('month(sell_date)'))
					->get();

		$arr_sellout = array();

		if(!empty($sellout))
		{
			$index_sellout = array();
			foreach($sellout as $row)
			{
				$index_sellout[$row->month_num]['month'] = substr($row->sell_month, 0, 3);
				$index_sellout[$row->month_num]['net'] = (integer)$row->net;
			}

			for($a=1;$a<=12;$a++)
			{
				$arr_sellout[$a]['label'] = !empty($index_sellout[$a]) ? $index_sellout[$a]['month'] : 0;
				$arr_sellout[$a]['y'] = !empty($index_sellout[$a]) ? $index_sellout[$a]['net'] : 0;
			}
		}
		

		// dd($arr_sellout);
		return $arr_sellout;
	}

	public static function chartFilter($report, $months, $chart_type)
	{
		$arr_chart = array();

		$index = 0;
		foreach($report as $data)
		{
			$arr_chart[$index]['type'] = $chart_type;
			$arr_chart[$index]['toolTipContent'] = "{name} : <b>{y}</b>";
			$arr_chart[$index]['showInLegend'] = true;
			$arr_chart[$index]['legendText'] = $data['area_name'];
			
			$arr_data = array();
			foreach($months as $key => $month)
			{
				$arr_data[$key]['y'] = (integer) $data["omzet_$key"];
				$arr_data[$key]['label'] = substr($month, 0, 3);
				$arr_data[$key]['name'] = $data['area_name'];
			}

			$arr_chart[$index]['dataPoints'] = $arr_data;

			$index++;
		}

    	$chart = \Helper::GenerateChart($arr_chart);

		return $chart;
	}

	public static function pieChartData($months, $data)
	{
		$months =  Self::unsetZeroData($months, $data);

		$index = 0;
		$chart = array();
		$arr_total = array();
		foreach($months as $key => $month)
		{
			$chart_data[$key][$index]['type'] = 'pie';
			$chart_data[$key][$index]['showInLegend'] = true;
			$chart_data[$key][$index]['toolTipContent'] = '{name} : <b>{y}</b>';
			$chart_data[$key][$index]['legendText'] = '{name}';

			$index_data = 0;
			$arr_pie = array();

			$total = 0;
			foreach($data as $row)
			{
				$total += $row["omzet_$key"];
				$arr_total[$key] = $total;
			}

			foreach($data as $row)
			{
				if($row["omzet_$key"] != 0){
					$percent_omzet = $row["omzet_$key"] / $arr_total[$key] * 100;

					$arr_pie[$index_data]['y'] = (integer)$row["omzet_$key"];
					$arr_pie[$index_data]['indexLabel'] = number_format($percent_omzet)."%";
					$arr_pie[$index_data]['name'] = $row['area_name'];
				}

				$index_data ++;
			}

			$chart_data[$key][$index]['dataPoints'] = $arr_pie;

			$chart[$key] = \Helper::GenerateChart($chart_data[$key], $month);

			$index ++;
		}

		return $chart;
	}

	public static function unsetZeroData($months, $data)
	{
		/* Unset zer data to hide other zero data in the specific month*/
		
		$arr_month = array();

		foreach($months as $key => $month)
		{
			
			foreach($data as $row)
			{
				if($row["omzet_$key"] != 0)
				{
					$arr_month[$key] = $month;
				}
			}
		}

		ksort($arr_month);

		return $arr_month;
	}
}
