<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class OutletType extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'outlet_types';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['name'];

	/* relationship */

	public function productTaste()
	{
		return $this->hasMany('App/ProductTaste');
	}

	/*
	Custom Function
	*/

	public static function rowExist($field, $param)
	{
		$branch = OutletType::where($field, '=', $param)->first();

		if(!empty($branch))
		{
			return $branch->id;
		}
		else
		{
			return FALSE;
		}
	}

	public static function ChecknInsert($field, $param)
	{
		$branch = OutletType::rowExist($field, $param);

		if($branch == FALSE)
		{
			$branch_insert = OutletType::create([$field => $param]);

			return $branch_insert->id;
		}
		else
		{
			return $branch;
		}
	}
}
