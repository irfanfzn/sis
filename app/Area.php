<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'areas';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	public function Depo()
	{
		return $this->hasMany('App\Depo');
	}

	public function ActualTarget()
	{
		return $this->hasMany('App\Area');
	}

	/*
	Custom function
	*/

	public static function getAreaByHandler()
	{
		if(\Helper::getUserRole() == "Area Handler")
		{
			$areas = Area::where('id', '=', \Helper::getAreaHandler());
		}
		else
		{
			$areas = Area::all();
		}

		return $areas;
	}
	
	public static function rowExist($field, $param)
	{
		$area = Area::where($field, '=', $param)->first();

		if(!empty($area))
		{
			return $area->id;
		}
		else
		{
			return FALSE;
		}
	}

	public static function ChecknInsert($field, $param)
	{
		$branch = Area::rowExist($field, $param);

		if($branch == FALSE)
		{
			$branch_insert = Area::create([$field => $param]);

			return $branch_insert->id;
		}
		else
		{
			return $branch;
		}
	}

}
