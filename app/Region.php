<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'regions';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['parent_region_id', 'name'];

	/*
	Relationships
	*/

}
