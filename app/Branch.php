<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'branches';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	/*
	Custom function
	*/

	public static function rowExist($field, $param)
	{
		$branch = Branch::where($field, '=', $param)->first();

		if(!empty($branch))
		{
			return $branch->id;
		}
		else
		{
			return FALSE;
		}
	}

	public static function ChecknInsert($field, $param)
	{
		$branch = Branch::rowExist($field, $param);

		if($branch == FALSE)
		{
			$branch_insert = Branch::create([$field => $param]);

			return $branch_insert->id;
		}
		else
		{
			return $branch;
		}
	}
}
