<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class OutletGroup extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'outlet_groups';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['name'];

	public static function rowExist($field, $param)
	{
		$outlet_group = OutletGroup::where($field, '=', $param)->first();

		if(!empty($outlet_group))
		{
			return $outlet_group->id;
		}
		else
		{
			return FALSE;
		}
	}

	public static function ChecknInsert($field, $param)
	{
		$outlet_group = OutletGroup::rowExist($field, $param);

		if($outlet_group == FALSE)
		{
			$outlet_group_insert = OutletGroup::create([$field => $param]);

			return $outlet_group_insert->id;
		}
		else
		{
			return $outlet_group;
		}
	}
}
