<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserArea extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_areas';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id', 'area_id'];

	public function User()
	{
		return $this->belongsTo('App\User');
	}

	public function Area()
	{
		return $this->belongsTo('App\Area');
	}

}
