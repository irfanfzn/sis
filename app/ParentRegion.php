<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentRegion extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'parent_regions';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	/*
	Relationships
	*/

}
