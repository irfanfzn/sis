<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'stocks';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
			'product_taste_id', 
			'depo_id',
			'stock', 
			'stock_date',
			'up1', 
		];

	public function stockAdjustment()
	{
		return $this->hasMany('App\StockAdjustment');
	}

	public function productTaste()
	{
		return $this->belongsTo('App\ProductTaste');
	}

	public function Depo()
	{
		return $this->belongsTo('App\Depo');
	}

	/*
	Custom function
	*/
	public static function rowExist($depo_id, $field, $param)
	{
		$stock = Stock::where('depo_id', '=', $depo_id)
				->where($field, '=', $param)->first();

		if(!empty($stock))
		{
			return ['stock' => $stock->stock, 'id' => $stock->id];
		}
		else
		{
			return "FALSE";
		}
	}

	public static function checkDouble($params = "")
	{
		$where_content = "";
		foreach($params as $field => $param)
		{
			$where_content .= "$field = $param and ";
		}
		$where_content = substr($where_content, 0, -5);

		$stock = Stock::whereRaw($where_content)
							->first();

		if(empty($stock))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public static function getStockReport($filters = "")
	{
		// if(!empty($filters['depo_ids']))
		// {
		// 	if(count($filters['depo_ids']) > 1)
		// 	{
		// 		$sum = "sum(stock)";
		// 	}
		// 	else
		// 	{
		// 		$sum = "stock";	
		// 	}
		// }
		$stock = Stock::select(\DB::raw("
				stocks.id as stocks_id,
				product_taste_id as variant_id,
			    sum(stock) as stock,
			    sum(up1) as up1,
			    sum((Select 
			            sum(up2)
			        from
			            stock_adjustments
			        where
			            stock_adjustments.stock_id = stocks_id)) as up2,
			    sum((Select 
			            sum(up3)
			        from
			            stock_adjustments
			        where
			            stock_adjustments.stock_id = stocks_id)) as up3,
			    product_tastes.name as variant,
			    outlet_types.name as outlet_type,
			    depos.name as depo,
			    (SELECT 
			            SUM(sell_outs.qty_total)
			        FROM
			            sell_outs
			        WHERE
			            date(sell_outs.sell_date) >= DATE_SUB(CURDATE(), INTERVAL 3 MONTH)
					and product_taste_id = variant_id) as stock_avg
			"))
			// ->join('stock_adjustments', 'stock_adjustments.stock_id', '=', 'stocks.id')
			->join('depos', 'depos.id', '=', 'stocks.depo_id')
			->join('areas', 'areas.id', '=', 'depos.area_id')
			->join('product_tastes', 'product_tastes.id', '=', 'stocks.product_taste_id')
			->join('products', 'products.id', '=', 'product_tastes.product_id')
			->join('outlet_types', 'outlet_types.id', '=', 'product_tastes.outlet_type_id');

		if(!empty($filters['depo_ids']))
		{
			$stock = $stock->whereIn('depo_id', $filters['depo_ids']);
		}

		if(!empty($filters['product_ids']))
		{
			$stock = $stock->whereIn('products.id', $filters['product_ids']);
		}

		if(!empty($filters['area_ids']))
		{
			$stock = $stock->whereIn('areas.id', $filters['area_ids']);
		}

		if(!empty($filters['outlet_type_ids']))
		{
			$stock = $stock->whereIn('outlet_types.id', $filters['outlet_type_ids']);
		}

		if(!empty($filters['product_taste_ids']))
		{
			foreach($filters['product_taste_ids'] as $key => $data)
			{
				if($key == 0)
				{
					$stock = $stock->Where('product_tastes.name', 'like', '%'. $data .'%');	
				}
				else
				{
					$stock = $stock->orWhere('product_tastes.name', 'like', '%'. $data .'%');		
				}
			}
		}

		$stock = $stock->groupBy('product_tastes.name')
					->groupBy('outlet_type')
					// ->groupBy('exp_date')
					->orderBy('product_tastes.name')
					->get();

			return $stock;
	}

	/*public static function checkDouble($params = "")
	{
		$where_content = "";
		foreach($params as $field => $param)
		{
			$where_content .= "$field = $param and ";
		}

		$where_content = substr($where_content, 0, -5);

		$sell_out = SellOut::whereRaw($where_content)
							->first();

		if(empty($sell_out))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}*/

}
