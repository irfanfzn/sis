<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SellIn extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sell_ins';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
			'distributor_id', 
			'depo_id', 
			'order_id',
			'outlet_type_id', 
			'product_taste_id', 
			'sellin_date', 
			'qty_ctn', 
			'tax',
			'status',
			'unit',
			'amount'
		];

	/* Custom function */

	public static function checkDouble($params = "")
	{
		$where_content = "";
		foreach($params as $field => $param)
		{
			$where_content .= "$field = $param and ";
		}
		$where_content = substr($where_content, 0, -5);

		$sell_out = Sellin::whereRaw($where_content)
							->first();

		if(empty($sell_out))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}


	public static function getFiltered($report_by = "areas", $month_counts = 0, $filters = "")
	{
		// dd($filters);
		$month = "";
		if($month_counts == 0)
		{
			for($a=1;$a<=12;$a++)
			{
				$month .= "sum(if(month(sellin_date) = $a, sell_ins.qty_ctn, 0)) as ctn_$a, 
							sum(if(month(sellin_date) = $a, sell_ins.amount, 0)) as amount_$a,";
			}	
		}
		else
		{
			foreach($month_counts as $a)
			{
				$month .= "sum(if(month(sellin_date) = $a, sell_ins.qty_ctn, 0)) as ctn_$a, 
							sum(if(month(sellin_date) = $a, sell_ins.amount, 0)) as amount_$a,";
			}
		}

		$month = substr($month, 0, -1);

		$sellin = SellIn::select(\DB::raw("
				$report_by.name as area_name, 
				$month"
				))
			->join('depos', 'depos.id', '=', 'sell_ins.depo_id')
			->join('distributors', 'distributors.id', '=', 'sell_ins.distributor_id')
			->join('areas', 'areas.id', '=', 'depos.area_id')
			->join('regions', 'regions.id', '=', 'areas.region_id')
			->join('parent_regions', 'parent_regions.id', '=', 'regions.parent_region_id')
			->join('outlet_types', 'outlet_types.id', '=', 'sell_ins.outlet_type_id')
			->join('product_tastes', 'product_tastes.id', '=', 'sell_ins.product_taste_id')
			->join('products', 'products.id', '=', 'product_tastes.product_id');

		if(\Helper::getUserRole() == "Area Handler")
		{
			$sellin = $sellin->where('areas.id', '=', \Helper::getAreaHandler());
		}

		$sellin = Self::filter($sellin, $filters);

		$sellin = $sellin->groupBy("$report_by.id")
			->get()
			->toArray();

		return $sellin;
	}

	public static function filter($sellin, $filters = "")
	{
		if(!empty($filters['year']))
		{
			$sellin = $sellin->whereRaw('year(sellin_date) = '.$filters['year']);
		}
		else
		{
			$sellin = $sellin->whereRaw('year(sellin_date) = 2015');
		}

		if(!empty($filters['parent_region_ids']))
		{
			$sellin = $sellin->whereIn('parent_regions.id', $filters['parent_region_ids']);
		}

		if(!empty($filters['region_ids']))
		{
			$sellin = $sellin->whereIn('regions.id', $filters['region_ids']);
		}

		if(!empty($filters['depo_ids']))
		{
			$sellin = $sellin->whereIn('depos.id', $filters['depo_ids']);
		}	

		if(!empty($filters['area_ids']))
		{
			$sellin = $sellin->whereIn('areas.id', $filters['area_ids']);
		}

		if(!empty($filters['outlet_type_ids']))
		{
			$sellin = $sellin->whereIn('outlet_types.id', $filters['outlet_type_ids']);
		}

		if(!empty($filters['product_ids']))
		{
			$sellin = $sellin->whereIn('products.id', $filters['product_ids']);
		}

		if(!empty($filters['distributor_ids']))
		{
			$sellin = $sellin->whereIn('distributors.id', $filters['distributor_ids']);
		}

		if(!empty($filters['product_taste_ids']))
		{
			$filters = $filters['product_taste_ids'];
			$sellin = $sellin->where(function($query) use ($filters)
			{
				foreach($filters as $key => $data)
				{
					if($key == 0)
					{
						$query->Where('product_tastes.name', 'like', '%'. $data .'%');	
					}
					else
					{
						$query->orWhere('product_tastes.name', 'like', '%'. $data .'%');		
					}
					
				}	
			});
		}

		return $sellin;
	}

	public static function getYear()
	{
		$year = SellIn::select(\DB::raw('YEAR(sellin_date) as sell_year'))
				->groupBy(\DB::raw('YEAR(sellin_date)'))
				->get();

		return $year;
	}

	public static function chartFilter($report, $months, $chart_type)
	{
		// dd($data);
		$arr_chart = array();

		$index = 0;
		foreach($report as $data)
		{
			$arr_chart[$index]['type'] = $chart_type;
			$arr_chart[$index]['toolTipContent'] = "{name} : <b>{y}</b>";
			$arr_chart[$index]['showInLegend'] = true;
			$arr_chart[$index]['legendText'] = $data['area_name'];
			
			$arr_data = array();
			foreach($months as $key => $month)
			{
				$arr_data[$key]['y'] = (integer) $data["amount_$key"];
				$arr_data[$key]['label'] = substr($month, 0, 3);
				$arr_data[$key]['name'] = $data['area_name'];
			}

			$arr_chart[$index]['dataPoints'] = $arr_data;

			$index++;
		}

    	$chart = \Helper::GenerateChart($arr_chart);

		return $chart;
	}

	public static function pieChartData($months, $data)
	{
		$months =  Self::unsetZeroData($months, $data);

		$index = 0;
		$chart = array();
		$arr_total = array();
		foreach($months as $key => $month)
		{
			$chart_data[$key][$index]['type'] = 'pie';
			$chart_data[$key][$index]['showInLegend'] = true;
			$chart_data[$key][$index]['toolTipContent'] = '{name} : <b>{y}</b>';
			$chart_data[$key][$index]['legendText'] = '{name}';

			$index_data = 0;
			$arr_pie = array();

			$total = 0;
			foreach($data as $row)
			{
				$total += $row["amount_$key"];
				$arr_total[$key] = $total;
			}

			foreach($data as $row)
			{
				if($row["amount_$key"] != 0){
					$percent_omzet = $row["amount_$key"] / $arr_total[$key] * 100;

					$arr_pie[$index_data]['y'] = (integer)$row["amount_$key"];
					$arr_pie[$index_data]['indexLabel'] = number_format($percent_omzet)."%";
					$arr_pie[$index_data]['name'] = $row['area_name'];
				}

				$index_data ++;
			}

			$chart_data[$key][$index]['dataPoints'] = $arr_pie;

			$chart[$key] = \Helper::GenerateChart($chart_data[$key], $month);

			$index ++;
		}

		return $chart;
	}

	public static function unsetZeroData($months, $data)
	{
		/* Unset zer data to hide other zero data in the specific month*/
		
		$arr_month = array();

		foreach($months as $key => $month)
		{
			
			foreach($data as $row)
			{
				if($row["amount_$key"] != 0)
				{
					$arr_month[$key] = $month;
				}
			}
		}

		ksort($arr_month);

		return $arr_month;
	}
}
