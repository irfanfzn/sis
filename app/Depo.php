<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Depo extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'depos';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	public function Area()
	{
		return $this->belongsTo('App\Area');
	}

	public function Stock()
	{
		return $this->hasMany('App\Stock');	
	}

	/*
	Custom function
	*/
	
	public static function rowExist($field, $param)
	{
		$depo = Depo::where($field, '=', $param)->first();

		if(!empty($depo))
		{
			return $depo->id;
		}
		else
		{
			return FALSE;
		}
	}

	public static function ChecknInsert($field, $param)
	{
		if($param == "")
			return FALSE;
		
		$depo = Depo::rowExist($field, $param);

		if($depo == FALSE)
		{
			$depo_insert = Depo::create([$field => $param]);

			return $depo_insert->id;
		}
		else
		{
			return $depo;
		}
	}
}
