<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/*
Before Auth
*/

Route::group(['middleware' => 'auth'], function()
{

	/*
	User Management
	*/

	Route::get('user', 'UserController@index');
	Route::get('user/create', 'UserController@create');
	Route::post('user/create', 'UserController@store');

	/* 
	Outlet route 
	*/

	Route::get('outlet', 'OutletController@index');
	Route::post('outlet', 'OutletController@search');
	Route::post('outlet/field', 'OutletController@getContentAjax');
	Route::post('outlet/search', 'OutletController@search');
	Route::get('outlet/create', 'OutletController@create');
	Route::post('outlet/create', 'OutletController@store');

	/*
	Product route
	*/

	Route::get('product', 'ProductController@index');
	Route::get('product/create', 'ProductController@create');
	Route::post('product/create', 'ProductController@store');
	Route::post('product/field', 'ProductController@getContentAjax');
	Route::post('product/search', 'ProductController@search');

	/*
	Product taste route
	*/

	Route::get('product_taste', 'ProductTasteController@index');
	Route::get('product_taste/create', 'ProductTasteController@create');
	Route::post('product_taste/create', 'ProductTasteController@store');
	Route::get('product_taste/import', 'ProductTasteController@getImport');
	Route::post('product_taste/import', 'ProductTasteController@postImport');
	Route::post('product_taste/field', 'ProductTasteController@getContentAjax');
	Route::post('product_taste/search', 'ProductTasteController@search');

	/*
	Brand route
	*/

	Route::get('brand', 'BrandController@index');
	Route::post('brand/field', 'BrandController@getContentAjax');
	Route::post('brand/search', 'BrandController@search');
	Route::get('brand/create', 'BrandController@create');
	Route::post('brand/create', 'BrandController@store');

	/*
	Taste route
	*/

	Route::get('taste', 'TasteController@index');
	Route::get('taste/create', 'TasteController@create');
	Route::post('taste/create', 'TasteController@store');

	/*
	Distributor route
	*/

	Route::get('distributor', 'DistributorController@index');
	Route::post('distributor/field', 'DistributorController@getContentAjax');
	Route::post('distributor/search', 'DistributorController@search');
	// Route::get('taste/create', 'TasteController@create');
	// Route::post('taste/create', 'TasteController@store');

	/*
	Area route
	*/

	Route::get('area', 'AreaController@index');
	Route::post('area/field', 'AreaController@getContentAjax');
	Route::post('area/search', 'AreaController@search');

	/*
	Depo route
	*/

	Route::get('depo', 'DepoController@index');	
	Route::post('depo/field', 'DepoController@getContentAjax');
	Route::post('depo/search', 'DepoController@search');

	/*
	Sell in route
	*/

	Route::get('sellin', 'SellinController@index');
	Route::post('sellin/create', 'SellinController@store');

	/*
	Sell out route
	*/

	Route::get('sellout', 'SelloutController@index');
	Route::get('sellout/create', 'SelloutController@create');
	Route::post('sellout/create', 'SelloutController@store');

	/*
	Stock route
	*/

	Route::get('stock', 'StockController@index');
	Route::post('stock/search', 'StockController@search');
	Route::get('stock/create', 'StockController@create');
	Route::post('stock/create', 'StockController@store');
	Route::get('stock/form', 'StockController@getForm');
	Route::post('stock/form', 'StockController@postForm');

	/*
	Target route
	*/

	Route::get('target', 'TargetController@index');
	Route::get('target/create', 'TargetController@create');
	Route::post('target/create', 'TargetController@store');
	Route::get('target/import', 'TargetController@getImport');
	Route::post('target/import', 'TargetController@postImport');

	/*
	Report route
	*/

	Route::get('report/sellin', 'SellinController@report');
	Route::post('report/sellin/filter', 'SellinController@filter');
	Route::get('report/sellout', 'SelloutController@report');
	Route::post('report/filter', 'SelloutController@filter');
	Route::get('report/trend/chart', 'TargetController@getTrendChart');
	Route::post('report/trend/chart', 'TargetController@postTrendChart');
	Route::get('report/trend', 'TargetController@getTrend');
	Route::post('report/trend', 'TargetController@postTrend');

	/*Clear Data*/

	Route::get('cleardata', function()
	{
		DB::table('distributors')->truncate();
		DB::table('outlets')->truncate();
		DB::table('distributors')->truncate();
		DB::table('sales')->truncate();
		DB::table('sales_promotions')->truncate();
		DB::table('sell_ins')->truncate();
		DB::table('sell_outs')->truncate();
		DB::table('stocks')->truncate();
		DB::table('actual_targets')->truncate();
		DB::table('best_estimate_targets')->truncate();
		DB::table('stock_adjustments')->truncate();

		return redirect('/home')->with('message', 'The data is cleared');
	});

	/*
	TEST
	*/
});

Route::post('upload', function()
{
	$state = \Input::get('state');
	$file = \Input::file('file');
	$destination = public_path().'/uploads';
	$filename = $file->getClientOriginalName();

	if($state == 1){
        $upload = $file->move($destination, $filename);
        return "Uploaded";
    }
    else{
        $delete = unlink($destination. '/'. $filename);
        return "Deleted";
    }

	return "success";
});

