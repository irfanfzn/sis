<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Distributor;
use Input;

use Illuminate\Http\Request;

class DistributorController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$distributor = Distributor::paginate(10);
		return view('distributor.index')->with('data', $distributor);
	}

	public function search()
	{
		$field = Input::get('field');
		$parameter = Input::get('parameter');

		$data = Distributor::where($field, 'like', '%'. $parameter .'%')->paginate(10);

		return view('distributor.index')->with('data', $data);
	}

	public function getContentAjax()
	{

		$field = Input::get('field');

		$data = Distributor::select(\DB::raw("distinct $field as content"))
						->get();

		return response()->json($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
