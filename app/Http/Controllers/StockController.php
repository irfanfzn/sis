<?php namespace App\Http\Controllers;

use Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Stock;
use App\DataFile;
use App\OutletType;
use App\ProductTaste;
use App\Product;
use App\Depo;
use App\Area;
use App\StockAdjustment;
use Validator;

use Illuminate\Http\Request;

class StockController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$depos = Depo::all();
		$areas = Area::all();
		$outlet_types = OutletType::all();
		$products = Product::all();
		$product_tastes = ProductTaste::groupBy('name')->get();
		
		$stocks = Stock::getStockReport();
		
		return view('stock.index')
				->with('stocks', $stocks)
				->with('products', $products)
				->with('depos', $depos)
				->with('areas', $areas)
				->with('outlet_types', $outlet_types)
				->with('product_tastes', $product_tastes);
	}

	public function search()
	{
		$depo_ids = Input::get('depos');
		$area_ids = Input::get('areas');
		$outlet_type_ids = Input::get('outlet_types');
		$product_ids = Input::get('products');
		$product_taste_ids = Input::get('product_tastes');

		$depos = Depo::all();
		$areas = Area::all();
		$outlet_types = OutletType::all();
		$products = Product::all();
		$product_tastes = ProductTaste::groupBy('name')->get();

		$filters = [
			'depo_ids' => $depo_ids, 
			'area_ids' => $area_ids, 
			'outlet_type_ids' => $outlet_type_ids, 
			'product_ids' => $product_ids, 
			'product_taste_ids' => $product_taste_ids,
			];

		$stocks = Stock::getStockReport($filters);
		
		return view('stock.index')
				->with('stocks', $stocks)
				->with('products', $products)
				->with('depos', $depos)
				->with('areas', $areas)
				->with('outlet_types', $outlet_types)
				->with('product_tastes', $product_tastes);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$depos = Depo::all();
		return view('stock.import')->with('depos', $depos);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = [
			'depo_id' => 'required',
			'method' => 'required',
			// 'excel' => 'required|mimes:xlsx, xls'
		];

		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
			return redirect('/stock/create')->withErrors($validator)->withInput();

		$depo_id = Input::get('depo_id');
		$method = Input::get('method');

		$path = 'uploads/stocks';

		$data_file = DataFile::uploadFile(Input::file('excel'), $path);

		// $read_excels = DataFile::readExcel('uploads/stock adjustment jateng.xlsx', 'F');
		$read_excels = DataFile::readExcel($data_file->path, 'F');
		$warning_message = array();
		
		$fields = [
			'TOTAL_ROW', 
			'ROW', 
			'DATE', 
			'OUTLET', 
			'KODE PRODUK', 
			'NAMA PRODUK', 
			'STOK FISIK (CTN)', 
			'STOK FISIK (PCS)', 
			'EXPIRED DATE'
			];

		$check_format = \Helper::CheckFormat($read_excels[3], $fields);
		// dd($read_excels);

		/* If format is did not match then remove file and row in data_fields */
		if($check_format == FALSE)
		{
			DataFile::deleteFile($data_file->id);
			return response()->json('Format did not match', 400);
		}
		/* End */


		foreach($read_excels as $excel)
		{
			$date_now = date_create(date('Y-m-d'));
			$date_exp = date_create($excel['expired date']);
			$outlet_type = OutletType::rowExist('name', $excel['outlet']);

			if($outlet_type != FALSE)
			{
				$check_spesial = \Helper::CheckSpecialProduct($excel['nama produk']);
				$check_spesial != FALSE ? $product_taste_name = $check_spesial : $product_taste_name = $excel['nama produk'];

				$product_taste = ProductTaste::outletTypeExist($outlet_type, 'name', $product_taste_name);

				if($product_taste != FALSE)
				{
					$diff = date_diff($date_now, $date_exp);

					$month_diff = $diff->y * 12 + $diff->m;

					$stock_up1 = 0;
					$stock_up2 = 0;
					$stock_up3 = 0;

					$stock_pcs = $excel['stok fisik (pcs)'] != 0 ? \helper::GenerateCtn($excel['stok fisik (pcs)'], $product_taste['pcs']) : 0;

					if($month_diff >= 4 && $month_diff <= 8)
					{
						$stock_up2 = $excel['stok fisik (ctn)'] + $stock_pcs;
					}
					elseif($month_diff <= 3)
					{
						$stock_up3 = $excel['stok fisik (ctn)'] + $stock_pcs;
					}
					else
					{
						$stock_up1 = $excel['stok fisik (ctn)'] + $stock_pcs;
					}

					$stock_ho = $excel['stok fisik (ctn)'] + $stock_pcs;

					$check_stock = Stock::rowExist($depo_id, 'product_taste_id', $product_taste['id']);

					if($check_stock == "FALSE")
					{
						$total_stock = $stock_up1 + $stock_up2 + $stock_up3;						

						$stock = Stock::create([
							'product_taste_id' => $product_taste['id'],
							'depo_id' => $depo_id,
							'stock' => $total_stock,
							'up1' => $stock_up1,
						]);

						$stock_id = $stock->id;
					}
					else
					{
						if($method == 'first_stock')
						{
							$total_stock = $check_stock['stock'] + $stock_up1 + $stock_up2 + $stock_up3;
							$total_up1 = $stock_up1;
						}
						elseif($method == 'adj')
						{
							$check_adj = StockAdjustment::getTotalAdj($check_stock['id']);

							$total_stock = $check_stock['stock'];
							$total_up2 = $stock_up2 + $check_adj['up2'];
							$total_up3 = $stock_up3 + $check_adj['up3'];
							$total_up1 = $total_stock - $total_up2 - $total_up3;
						}
						else
						{
							$total_stock = $check_stock['stock'] - $stock_ho;
							$total_up1 = $total_stock - $stock_up2 - $stock_up3;
						}

						$stock = Stock::where('product_taste_id', '=', $product_taste['id'])
										->where('depo_id', '=', $depo_id);

						$stock->update([
							'stock' => $total_stock, 
							'up1' => $total_up1
							]);

						$stock_id = $check_stock['id'];
					}

					$check_stock_exp = StockAdjustment::rowExist($excel['expired date'], $product_taste['id'], $depo_id);

					if($check_stock_exp == "FALSE")
					{
						$stock_adj = StockAdjustment::create([
								'stock_id' => $stock_id,
								'exp_date' => $excel['expired date'],
								'up2' => $stock_up2,
								'up3' => $stock_up3
							]);
					}
					else
					{
						// echo "$stock_up3 <br>";
						$delete = StockAdjustment::where('stock_id', '=', $check_stock_exp)->delete();
						$stock_adj = StockAdjustment::create([
								'stock_id' => $stock_id,
								'exp_date' => $excel['expired date'],
								'up2' => $stock_up2,
								'up3' => $stock_up3
							]);
					}
					
				}
				else
				{
					$warning_message[] = "$product_taste_name in row ". $excel['row'] ." not exist in database";
				}
			}
			else
			{
				$warning_message[] = $excel['outlet']. " in row ". $excel['row'] ." not exist in database";
			}

		}
		
		return response()->json(!empty($warning_message) ? $warning_message : "Success");
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getForm()
	{
		$product_tastes = ProductTaste::all();

		$depos = Depo::all();

		return view('stock.create')
				->with('product_tastes', $product_tastes)
				->with('depos', $depos);
	}

	public function postForm()
	{
		$depo_id = Input::get('depo_id');
		$product_taste_id = Input::get('product_taste_id');
		$stock = Input::get('stock');
		$up1 = Input::get('up1');
		$up2 = Input::get('up2');
		$up3 = Input::get('up3');

		$rules = [
			'product_taste_id' => 'required',
			'depo_id' => 'required',
			'stock' =>'required|numeric', 
			'up1' => 'required|numeric', 
			'up2' => 'required|numeric',
			'up3' => 'required|numeric'
			];

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails())
			return redirect('/stock/form')->withErrors($validator)->withInput();

		Stock::create([
				'product_taste_id' => $product_taste_id,
				'depo_id' => $depo_id,
				'stock' => $stock,
				'up1' => $up1,
				'up2' => $up2,
				'up3' => $up3,
			]);

		return redirect('/stock/form')->with('message', 'The new stock adjustment is added');
	}
}
