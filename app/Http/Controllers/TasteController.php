<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Taste;
use Input;
use Validator;

use Illuminate\Http\Request;

class TasteController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tastes = Taste::all();

		return view('taste.index')->with('tastes', $tastes);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('taste.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = ['name' => 'required|max:255', 'code' => 'required|max:3'];

		$validator = Validator::make(Input::all(), $rules);

		if($validator->passes())
		{
			$brand = Taste::create(['name' => Input::get('name'), 'code' => Input::get('code')]);

			return redirect('/taste/create')->with('message', 'Taste varian is successfuly created');
		}
		else
		{
			return redirect('/taste/create')->withError($validator->errors())->withInput();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
