<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Brand;
use App\Product;
use Input;
use Validator;

use Illuminate\Http\Request;

class ProductController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = Product::paginate(10);

		return view('product.index')->with('products', $products);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$brands = Brand::all();

		return view('product.create')->with('data', $brands);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = ['name' => 'required|max:255', 'code' => 'required|max:3', 'brand_id' => 'Required'];

		$validator = Validator::make(Input::all(), $rules);

		if($validator->passes())
		{
			$brand = Product::create(
				[
					'name' => Input::get('name'), 
					'code' => Input::get('code'), 
					'brand_id' => Input::get('brand_id')
				]
			);

			return redirect('/product/create')->with('message', 'Brand is successfuly created');
		}
		else
		{
			return redirect('/product/create')->withError($validator->errors())->withInput();
		}
	}

	public function search()
	{
		$field = Input::get('field');
		$parameter = Input::get('parameter');

		$data = Product::join('brands', 'brands.id', '=', 'products.brand_id')
						->where($field, 'like', '%'. $parameter .'%')->paginate(10);

		return view('product.index')->with('products', $data);
	}

	public function getContentAjax()
	{

		$field = Input::get('field');

		$data = Product::select(\DB::raw("distinct $field as content"))
						->join('brands', 'brands.id', '=', 'products.brand_id')
						->get();

		return response()->json($data);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
