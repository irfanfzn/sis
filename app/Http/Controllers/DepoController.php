<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Depo;
use App\Http\Controllers\Controller;
use Input;

use Illuminate\Http\Request;

class DepoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$depo = Depo::select('areas.name as area_name', 'depos.name as depo_name', 'depos.created_at')
					->join('areas', 'areas.id', '=', 'depos.area_id')
					->paginate(10);

		return view('depo.index')->with('data', $depo);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	public function search()
	{
		$field = Input::get('field');
		$parameter = Input::get('parameter');

		$depo = Depo::select('areas.name as area_name', 'depos.name as depo_name')
					->join('areas', 'areas.id', '=', 'depos.area_id')
					->where($field, 'like', '%'. $parameter .'%')->paginate(10);

		return view('depo.index')->with('data', $depo);
	}

	public function getContentAjax()
	{

		$field = Input::get('field');

		$outlets = Depo::select(\DB::raw("distinct $field as content"))
						->join('areas', 'areas.id', '=', 'depos.area_id')
						->get();

		return response()->json($outlets);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
