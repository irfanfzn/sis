<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DataFile;
use App\Outlet;
use Input;

use Illuminate\Http\Request;

class OutletController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$outlets = Outlet::paginate(10);	

		return view('outlet.index')->with('data', $outlets);
	}

	public function search()
	{
		$field = Input::get('field');
		$parameter = Input::get('parameter');

		$outlets = Outlet::where($field, 'like', '%'. $parameter .'%')->paginate(10);

		return view('outlet.index')->with('data', $outlets);
	}

	public function getContentAjax()
	{

		$field = Input::get('field');

		$outlets = Outlet::select(\DB::raw("distinct $field as content"))->get();

		return response()->json($outlets);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('outlet.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$path = 'uploads/outlets';

		$data_file = DataFile::uploadFile(Input::file('excel'), $path);

		$read_excels = DataFile::readExcel($data_file->path, 'AG');

		if(file_exists('uploads/log_file.txt'))
		{
			file_put_contents('uploads/log_file.txt', "");
		}

		/* Check excel format */
		$fields = [
			'TOTAL_ROW',
			'ROW',
			'KODE OUTLET', 
			'NAMA OUTLET', 
			'ALAMAT OUTLET', 
			'KELOMPOK OUTLET', 
			'JENIS OUTLET', 
			'TYPE OUTLET', 
			'DC'
			];

		$check_format = \Helper::CheckFormat($read_excels[3], $fields);

		/* If format is did not match then remove file and row in data_fields */
		if($check_format == FALSE)
		{
			DataFile::deleteFile($data_file->id);
			return response()->json('Format did not match', 400);
		}
		/* End */

		$error_message = array();

		foreach($read_excels as $excel)
		{
			$row_exist = Outlet::rowExist('code', $excel['kode outlet']);

			if($row_exist == FALSE)
			{
				$insert_outlet = Outlet::create([
					'code' => $excel['kode outlet'],
					'name' => $excel['nama outlet'],
					'address' => $excel['alamat outlet'],
					'group' => $excel['kelompok outlet'],
					'type' => $excel['jenis outlet'],
					'dc' => $excel['dc'],
					'data_file_id' => $data_file->id
					]
				);
			}
			else
			{
				$message = "Code ". $excel['kode outlet']. " with outlet name ". $excel['nama outlet']. " is already exist \n";
				$error_message[] = $message;
				// $file_put = file_put_contents('uploads/log_file.txt', $message,  FILE_APPEND | LOCK_EX);
			}
		}

		return response()->json(!empty($error_message) ? $error_message : "");
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
