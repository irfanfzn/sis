<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Brand;
use Validator;
use Input;

use Illuminate\Http\Request;

class BrandController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$brand = Brand::all();

		return view('brand.index')->with('data', $brand);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('brand.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = ['name' => 'required|max:255', 'status' => 'required|max:30'];

		$validator = Validator::make(Input::all(), $rules);

		if($validator->passes())
		{
			$brand = Brand::create(['name' => Input::get('name'), 'status' => Input::get('status')]);

			return redirect('/brand/create')->with('message', 'Brand is successfuly created');
		}
		else
		{
			return redirect('/brand/create')->withError($validator->errors())->withInput();
		}
	}

	public function search()
	{
		$field = Input::get('field');
		$parameter = Input::get('parameter');

		$brand = Brand::where($field, 'like', '%'. $parameter .'%')->paginate(10);

		return view('brand.index')->with('data', $brand);
	}

	public function getContentAjax()
	{

		$field = Input::get('field');

		$outlets = Brand::select(\DB::raw("distinct $field as content"))->get();

		return response()->json($outlets);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
