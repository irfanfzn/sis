<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DataFile;
use App\OutletType;
use App\Product;
use App\ProductTaste;
use App\Taste;
use Input;

use Illuminate\Http\Request;

class ProductTasteController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$product_tastes = ProductTaste::paginate(10);
		return view('product_taste.index')->with('product_tastes', $product_tastes);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$products = Product::all();

		$outlet_types = OutletType::all();

		$tastes = Taste::all();

		return view('product_taste.create')
					->with('products', $products)
					->with('outlet_types', $outlet_types)
					->with('tastes', $tastes);
	}

	public function getImport()
	{
		return view('product_taste.import');
	}

	public function postImport()
	{
		$path = 'uploads/products';

		$data_file = DataFile::uploadFile(Input::file('excel'), $path);

		// $read_excels = DataFile::readExcel('uploads/Spek untuk konversi pcs ke ctn.xlsx', 'D');
		$read_excels = DataFile::readExcel($data_file->path, 'D');
		
		$fields = [
			'TOTAL_ROW',
			'ROW',
			'OUTLET',
			'PRODUK',
			'VARIANT',
			'SPEK',
			'PCS'
			];

		$check_format = \Helper::CheckFormat($read_excels[3], $fields);

		/* If format is did not match then remove file and row in data_fields */
		if($check_format == FALSE)
		{
			// DataFile::deleteFile($data_file->id);
			return response()->json('Format did not match', 400);
		}
		/* End */


		foreach($read_excels as $excel)
		{
			$name = $excel['produk'].' '.$excel['variant'];

			$check_product = Product::rowExist('name', $excel['produk']);
			
			if($check_product != FALSE)
				$product = $check_product;

			$taste = Taste::rowExist('name', $excel['variant']);

			$outlet_type = OutletType::ChecknInsert('name', $excel['outlet']);

			$product_taste = ProductTaste::outletTypeExist($outlet_type, 'name', $name);

			if($product_taste == FALSE)
			{
				$code = \Helper::GenerateCode($product, $taste, $excel['outlet']);

				$data = [
					'product_id' => $product, 
					'outlet_type_id' => $outlet_type, 
					'code' => $code, 
					'name' => $name,
					'pcs' => $excel['pcs']
				];

				ProductTaste::create($data);
			}
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$product_id = Input::get('product_id');
		$outlet_type_id = Input::get('outlet_type_id');
		$taste_id = Input::get('taste_id');
		$name = Input::get('name');
		$pcs = Input::get('pcs');

		$outlet_type = OutletType::where('id', '=', $outlet_type_id)->first()->name;
		
		$code = \Helper::GenerateCode($product_id, $taste_id, $outlet_type);

		$insert = ProductTaste::create([
			'product_id' => $product_id,
			'outlet_type_id' => $outlet_type_id,
			'code' => $code,
			'name' => $name,
			'pcs' => $pcs
			]);

		return redirect('/product_taste/create')->with('message', 'The product taste is successfully added');
	}

	public function search()
	{
		$field = Input::get('field');
		$parameter = Input::get('parameter');

		$data = ProductTaste::join('products', 'products.id', '=', 'product_tastes.product_id')
						->join('outlet_types', 'outlet_types.id', '=', 'product_tastes.outlet_type_id')
						->where($field, 'like', '%'. $parameter .'%')->paginate(10);

		return view('product_taste.index')->with('product_tastes', $data);
	}

	public function getContentAjax()
	{

		$field = Input::get('field');

		$data = ProductTaste::select(\DB::raw("distinct $field as content"))
						->join('products', 'products.id', '=', 'product_tastes.product_id')
						->join('outlet_types', 'outlet_types.id', '=', 'product_tastes.outlet_type_id')
						->get();

		return response()->json($data);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
