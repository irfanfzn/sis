<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Helfull\CanvasJS\Chart;
use Helfull\CanvasJS\Chart\ChartData;
use Helfull\CanvasJS\Chart\DataPoint;
use Validator;
use Input;
use App\BestEstimateTarget;
use App\DataFile;
use App\ActualTarget;
use App\Product;
use App\Area;
use App\OutletType;
use App\SellOut;
use App\OutletGroup;

class TargetController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$be = BestEstimateTarget::orderBy('month_num')->paginate(10);
		return view('target.index')->with('data', $be);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('target.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */


	public function store()
	{
		$month = Input::get('month');
		$year = Input::get('year');
		$be = Input::get('best_estimate');
		$be_exist = BestEstimateTarget::where('target_month', '=', $month)->where('target_year', '=', $year)->get();
		
		$month_num = \Helper::MonthNumber($month);

		if(empty($be_exist))
			return redirect('/target/create')->withErrors(['message' => 'Month and year is already in database'])->withInput();

		$rules = ['month' =>'Required', 'year' => 'numeric', 'best_estimate' => 'numeric'];
		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails())
			return redirect('/target/create')->withErrors($validator)->withInput();

		$be_insert = BestEstimateTarget::create([
			'month_num' => $month_num,
			'target_month' => $month, 
			'target_year' => $year, 
			'base_estimate' => $be
			]);

		return redirect('/target/create')->with('message', 'Best estimate data is added');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getImport()
	{
		return view('target.import');
	}

	public function postImport()
	{
		$year = Input::get('year');
		$target_type = Input::get('target_type');

		$rules = [
			'year' => 'numeric|required',
			'excel' => 'required',
			'target_type' => 'required'
		];

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails())
			return redirect('/target/import')->withErrors($validator)->withInput();


		$path = 'uploads/target';

		$data_file = DataFile::uploadFile(Input::file('excel'), $path);

		// $read_excels = DataFile::readExcel('uploads/SELL OUT JATENG baru.xlsx', 'AJ');
		$read_excels = DataFile::readExcel($data_file->path, 'F');

		$warning_message = array();

		/* Checking excel format */
		$field = [
			'TOTAL_ROW', 
			'ROW', 
			'PRODUCT', 
			'JENIS OUTLET', 
			'KELOMPOK OUTLET', 
			'TYPE OUTLET', 
			'AREA', 
			'BULAN', 
			$target_type != 'be' ? 'TARGET' : 'BE'
		];
		
		$check_format = \Helper::CheckFormat($read_excels[2], $field);

		if($check_format == FALSE)
		{
			// DataFile::deleteFile($data_file->id);	
			return response()->json('Format did not match', 400);
		}

		/* Start parsing */
		foreach($read_excels as $excel)
		{
			$month = ucwords(strtolower($excel['bulan']));

			$month_num = \Helper::MonthNumber($month);

			$product = Product::rowExist('name', $excel['product']);
			if($product == FALSE){
				$warning_message[] = "This Product ". $excel['product'] ." is not in database on row ".$excel['row'];
				continue;
			}

			$area = Area::rowExist('name', $excel['area']);
			if($area == FALSE){
				$warning_message[] = "This area ". $excel['area'] ." is not in database on row ".$excel['row'];
				continue;
			}

			$outlet_type = OutletType::rowExist('name', $excel['type outlet']);
			if($outlet_type == FALSE){
				$warning_message[] = "This outlet type ". $excel['type outlet'] ." is not in database on row ".$excel['row'];
				continue;
			}

			$outlet_group = OutletGroup::ChecknInsert('name', $excel['kelompok outlet']);


			if($target_type == 'actual')
			{
				$var_target = [
					'product_id' => $product,
					'area_id' => $area,
					'outlet_type_id' => $outlet_type,
					'outlet_group_id' => $outlet_group,
					'target_month' => \Helper::StringFormat($month),
					'target_year' => $year,
					'target' => $excel['target']
				];

				$check_target = ActualTarget::checkDouble($var_target);

				if($check_target == TRUE)
				{
					$create = ActualTarget::create([
							'product_id' => $product,
							'area_id' => $area,
							'outlet_type_id' => $outlet_type,
							'outlet_group_id' => $outlet_group,
							'month_num' => $month_num,
							'target_month' => $month,
							'target_year' => $year,
							'target' => $excel['target']
						]);
				}
				else
				{
					$warning_message[] = "There is double data in row ".$excel['row'];
				}
			}
			else
			{
				$var_target = [
					'product_id' => $product,
					'area_id' => $area,
					'outlet_type_id' => $outlet_type,
					'outlet_group_id' => $outlet_group,
					'target_month' => \Helper::StringFormat($month),
					'target_year' => $year,
					'target' => $excel['be']
				];

				$check_target = BestEstimateTarget::checkDouble($var_target);

				if($check_target == TRUE)
				{
					$create = BestEstimateTarget::create([
							'product_id' => $product,
							'area_id' => $area,
							'outlet_type_id' => $outlet_type,
							'outlet_group_id' => $outlet_group,
							'month_num' => $month_num,
							'target_month' => $month,
							'target_year' => $year,
							'target' => $excel['be']
						]);
				}
				else
				{
					$warning_message[] = "There is double data in row ".$excel['row'];
				}
			}
		}

		return response()->json(!empty($warning_message) ? $warning_message : "Success");
	}

	public function getTrend()
	{
		$month_lists = \Helper::MonthFilter();

		$products = Product::all();
		$areas = Area::all();
		$outlet_types = OutletType::all();
		$outlet_groups = OutletGroup::all();

		$targets = ActualTarget::getFiltered();

		// dd($targets);

		return view('report.trend.index')
				->with('data', $targets)
				->with('products', $products)
				->with('areas', $areas)
				->with('outlet_types', $outlet_types)
				->with('outlet_groups', $outlet_groups)
				->with('months', $month_lists);
	}

	public function postTrend()
	{
		$months = Input::get('months');
		$area_ids = Input::get('areas');
		$outlet_type_ids = Input::get('outlet_types');
		$product_ids = Input::get('products');
		$outlet_group_ids = Input::get('outlet_groups');
		// $report_type = Input::get('report_type');
		// $year = Input::get('year');

		$month_lists = \Helper::MonthFilter($months);

		$products = Product::all();
		$areas = Area::all();
		$outlet_types = OutletType::all();
		$outlet_groups = OutletGroup::all();

		$filters = [
			// 'year'=> $year,
			'area_ids' => $area_ids, 
			'outlet_type_ids' => $outlet_type_ids, 
			'product_ids' => $product_ids, 
			'outlet_group_ids' => $outlet_group_ids,
			];

		$targets = ActualTarget::getFiltered($months, $filters);

		// dd($targets);

		return view('report.trend.index')
				->with('data', $targets)
				->with('products', $products)
				->with('areas', $areas)
				->with('outlet_types', $outlet_types)
				->with('outlet_groups', $outlet_groups)
				->with('months', $month_lists);
	}

	public function getTrendChart()
	{
		$chart_type = "line";
		$year = Date('Y');
		$year_diff = $year - 1;

		$products = Product::all();
		$areas = Area::all();
		$outlet_types = OutletType::all();
		$outlet_groups = OutletGroup::all();
		
		$act_target_data = ActualTarget::chartTarget();

		$sellout_data = Sellout::chartTarget();

		$filters['year'] = $year_diff;
		$sellout_data_b = Sellout::chartTarget($filters);

		$be_data = BestEstimateTarget::chartTarget();
		$arr_trend = [
				'actual_target' => $act_target_data,
				'best_estimate' => $be_data,
				'sellout_data' => $sellout_data,
				'sellout_data_b' => $sellout_data_b
			];

		$chart_data = \Helper::TrendChartData($arr_trend, $chart_type, $year);

	   	$chart = \Helper::GenerateChart($chart_data);
		
		return view('report.trend.chart')
				->with('chart', $chart)
				->with('products', $products)
				->with('areas', $areas)
				->with('outlet_types', $outlet_types)
				->with('outlet_groups', $outlet_groups);
	}

	public function postTrendChart()
	{
		$chart_type = !empty(Input::get('report_type')) ? Input::get('report_type') : 'line';
		$year = Date('Y');
		$year_diff = $year - 1;

		$area_ids = Input::get('areas');
		$outlet_type_ids = Input::get('outlet_types');
		$product_ids = Input::get('products');
		$outlet_group_ids = Input::get('outlet_groups');

		$products = Product::all();
		$areas = Area::all();
		$outlet_types = OutletType::all();
		$outlet_groups = OutletGroup::all();
		
		$filters = [
			'area_ids' => $area_ids,
			'outlet_type_ids' => $outlet_type_ids,
			'product_ids' => $product_ids,
			'outlet_group_ids' => $outlet_group_ids,
			'year' => $year
		];

		$act_target_data = ActualTarget::chartTarget($filters);

		$sellout_data = Sellout::chartTarget($filters);

		$be_data = BestEstimateTarget::chartTarget($filters);

		$filters['year'] = $year_diff;
		$sellout_data_b = Sellout::chartTarget($filters);

		$arr_trend = [
				'actual_target' => $act_target_data,
				'best_estimate' => $be_data,
				'sellout_data' => $sellout_data,
				'sellout_data_b' => $sellout_data_b
			];

		$chart_data = \Helper::TrendChartData($arr_trend, $chart_type, $year);

	   	$chart = \Helper::GenerateChart($chart_data);
		
		return view('report.trend.chart')
				->with('chart', $chart)
				->with('products', $products)
				->with('areas', $areas)
				->with('outlet_types', $outlet_types)
				->with('outlet_groups', $outlet_groups);
	}
}
