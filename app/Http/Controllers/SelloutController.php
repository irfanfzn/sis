<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Helfull\CanvasJS\Chart;
use Helfull\CanvasJS\Chart\ChartData;
use Helfull\CanvasJS\Chart\DataPoint;
use App\DataFile;
use App\SellOut;
use App\Product;
use App\ProductTaste;
use App\Taste;
use App\Branch;
use App\Distributor;
use App\Sale;
use App\SalesPromotion;
use App\Area;
use App\Outlet;
use App\OutletType;
use App\Stock;
use App\Depo;
use App\Region;
use App\ParentRegion;
use App\OutletGroup;
use App\OutletTypeDetail;
use Input;

use Illuminate\Http\Request;

class SelloutController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('sellout.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('sellout.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$path = 'uploads/sellout';

		$data_file = DataFile::uploadFile(Input::file('excel'), $path);

		// $read_excels = DataFile::readExcel('uploads/SELL OUT JATENG baru.xlsx', 'AJ');
		$read_excels = DataFile::readExcel($data_file->path, 'AJ');

		$warning_message = array();

		/* Checking excel format */
		$field = [
			'TOTAL_ROW', 
			'ROW', 
			'YEAR', 
			'MONTH', 
			'DATE', 
			'WEEK', 
			'TANGGAL INPUT DATA', 
			'DEPO', 
			'AREA COVERAGE', 
			'DISTRIBUTOR', 
			'NAMA SALES', 
			'SALES EXCLUSIVE *)', 
			'ASPR', 
			'AREA', 
			'KODE PRODUK', 
			'BRAND', 
			'PRODUK', 
			'VARIAN RASA', 
			'KODE OUTLET', 
			'NAMA OUTLET/DC', 
			'ALAMAT OUTLET/DC', 
			'KELOMPOK OUTLET', 
			'JENIS OUTLET', 
			'TYPE OUTLET', 
			'NAMA DC', 
			'QTY (CTN)', 
			'QTY (PCS)', 
			'JUMLAH', 
			'RETUR (CTN)', 
			'RETUR (PCS)', 
			'NILAI RETUR (RP)', 
			'NET SALES (CTN)', 
			'NET SALES (PCS)', 
			'BERSIH', 
			'HARGA RBP EXC PPN (CTN)', 
			'HARGA RBP EXC PPN (PCS)', 
			'OMZET RBP EXC PPN (RP)', 
			'RETUR RBP EXC PPN (RP)', 
			'OMZET'
		];
		
		$check_format = \Helper::CheckFormat($read_excels[2], $field);

		if($check_format == FALSE)
		{
			// DataFile::deleteFile($data_file->id);	
			return response()->json('Format did not match', 400);
		}

		/* Start parsing */
		foreach($read_excels as $excel)
		{
			$product = Product::rowExist('name', $excel['produk']);
			
			$outlet_type = OutletType::rowExist('name', $excel['type outlet']);
			if($excel['type outlet'] != "")
			{
				if($outlet_type == FALSE)
				{
					$warning_message[] = "This Outlet type ". $excel['type outlet'] ." is not in database on row ".$excel['row'];
					continue;
				}	
			}
			else
			{
				$warning_message[] = "This Outlet type is blank in row ".$excel['row'];
				continue;
			}
			

			$check_spesial = \Helper::CheckSpecialProduct($excel['varian rasa']);
			$check_spesial != FALSE ? $product_taste_name = $check_spesial : $product_taste_name = $excel['varian rasa'];

			$product_taste = ProductTaste::outletTypeExist($outlet_type, 'name', $product_taste_name);
			if($product_taste == FALSE){
				$warning_message[] = "This Variant ". $product_taste_name ." is not in database on row ".$excel['row']; 
				continue;
			}

			/* Check Depo */
			$depo = Depo::rowExist('name', $excel['depo']);
			if($depo == FALSE){
				$warning_message[] = "This Depo ". $excel['depo'] ." is not in database on row ".$excel['row']; 
				continue;
			}

			/* Check and insert distributor */
			$distributor = Distributor::ChecknInsert('name', $excel['distributor']);

			/* Check and insert Sales */
			$sale = Sale::ChecknInsert('name', $excel['nama sales']);

			/* Check and insert ASPR/Area Sales Promotion sales_promotions */
			$sales_promotion = SalesPromotion::ChecknInsert('name', $excel['aspr']);

			/* Check and insert areas */
			$area_text = ucwords(strtolower($excel['area']));
			$area = Area::rowExist('name', $area_text);
			if($area == FALSE){
				$alert_message = "The area ". $excel['area'] ." is not registered in the database on row ".$excel['row']; 
				return response()->json($alert_message);
			}
				
			/* Check Outlets */
			$outlet_group = OutletGroup::ChecknInsert('name', $excel['kelompok outlet']);

			$outlet_type_detail = OutletTypeDetail::ChecknInsert('name', $excel['jenis outlet']);

			$outlet = Outlet::rowExist('code', $excel['kode outlet']);
			$outlet_id = 0;

			if($outlet == FALSE)
			{
				$outlet_insert = Outlet::create([
						'outlet_type_id' => $outlet_type,
						'outlet_group_id' => $outlet_group,
						'outlet_type_detail_id' => $outlet_type_detail,
						'code' => $excel['kode outlet'],
						'name' => $excel['nama outlet/dc'],
						'address' => $excel['alamat outlet/dc'] != "" ? $excel['alamat outlet/dc'] : ""
					]);

				$outlet_id = $outlet_insert->id;
			}
			else
			{
				$outlet_id = $outlet;
			}
			// $outlet_id = $outlet != FALSE ? $outlet : 0;

			/* Check and insert sell_outs */
			$var_sellout = [
				'sell_date' => \Helper::StringFormat($excel['date']),
				'depo_id' => $depo,
				'sale_id' => $sale,
				'area_id' => $area,
				'outlet_id' => $outlet_id,
				'qty_ctn' => $excel['qty (ctn)'],
				'qty_pcs' => $excel['qty (pcs)'],
				'return_pcs' => $excel['retur (pcs)'],
				'return_ctn' => $excel['retur (ctn)'],
				'net' => $excel['bersih'],
				'outlet_type_id' => $outlet_type,
				'product_taste_id' => $product_taste['id']
			];

			$check_sellout = SellOut::checkDouble($var_sellout);

			if($check_sellout == TRUE)
			{
				/*Stock convertion*/
				$stock_pcs = $excel['qty (pcs)'] != 0 ? \helper::GenerateCtn($excel['qty (pcs)'], $product_taste['pcs']) : 0;
				$total_qty = $excel['qty (ctn)'] + $stock_pcs;

				$return_pcs = $excel['retur (pcs)'] != 0 ? \helper::GenerateCtn($excel['retur (pcs)'], $product_taste['pcs']) : 0;
				$total_return = $excel['retur (ctn)'] + $return_pcs;


				$data = [
						'product_taste_id' => !empty($product_taste['id']) ? $product_taste['id'] : 0,
						'depo_id' => $depo,
						'outlet_type_id' => $outlet_type,
						'sale_id' => !empty($sale) ? $sale : 0,
						'sales_promotion_id' => !empty($sales_promotion) ? $sales_promotion : 0,
						'area_id' => !empty($area) ? $area : 0,
						'sell_date' => $excel['date'],
						'qty_ctn' => $excel['qty (ctn)'],
						'qty_pcs' => $excel['qty (pcs)'],
						'qty_converted_pcs' => $stock_pcs,
						'qty_total' => $total_qty,
						'return_ctn' => $excel['retur (ctn)'],
						'return_pcs' => $excel['retur (pcs)'],
						'return_converted_pcs' => $return_pcs,
						'return_total' => $total_return,
						'omzet' => $excel['omzet rbp exc ppn (rp)'],
						'net' => $excel['bersih'],
						'distributor_id' => !empty($distributor) ? $distributor : $warning_message[] = \Helper::EmptyWarning('Distributor', $excel['row']),
						'outlet_id' => $outlet_id
					];

				$insert_sellout = SellOut::create($data);

				$check_stock = Stock::rowExist($depo, 'product_taste_id', $product_taste['id']);
				$total_stock = !empty($check_stock['stock']) ? $check_stock['stock'] - $total_qty + $total_return : 0 - $total_qty + $total_return;
				
				if($check_stock == "FALSE")
				{
					Stock::create([
						'product_taste_id' => $product_taste['id'],
						'depo_id' => $depo,
						'stock' => $total_stock,
						'up1' => $total_stock
					]);
				}
				else
				{
					$stock = Stock::where('product_taste_id', '=', $product_taste['id'])
									->where('depo_id', '=', $depo);

					$stock_data = $stock->first();
					
					$adj = \App\StockAdjustment::getTotalAdj($stock_data->id);

					$stock_up1 = $total_stock - $adj['up2'] - $adj['up3'];

					// $stock = Stock::where('product_taste_id', '=', $product_taste['id']);
					$stock->update(['stock' => $total_stock, 'up1' => $stock_up1]);
				}
			}
			else
			{
				$warning_message[] = "There is double data in row ".$excel['row'];
			}

		}

		return response()->json(!empty($warning_message) ? $warning_message : "Success");
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function report()
	{
		// dd($this->month_name);
		$years = Sellout::getYear();

		$month_lists = \Helper::MonthFilter();

		/*Filter parameters*/
		$parent_regions = ParentRegion::all();

		$regions = Region::all();

		$depos = Depo::all();

		$areas = Area::all();

		$distributors = Distributor::all();

		$outlet_types = OutletType::all();

		$products = Product::all();

		$sales = Sale::all();

		$outlet_groups = OutletGroup::all();

		$outlet_type_details = OutletTypeDetail::all();

		$product_tastes = ProductTaste::groupBy('name')->get();
		/*--end--*/

		$report_sellout = SellOut::getSelloutFiltered();
		
		return view('report.sellout.index')
				->with('data', $report_sellout)
				->with('months', $month_lists)
				->with('years', $years)
				->with('parent_regions', $parent_regions)
				->with('regions', $regions)
				->with('depos', $depos)
				->with('areas', $areas)
				->with('distributors', $distributors)
				->with('outlet_types', $outlet_types)
				->with('products', $products)
				->with('sales', $sales)
				->with('outlet_groups', $outlet_groups)
				->with('outlet_type_details', $outlet_type_details)
				->with('product_tastes', $product_tastes);
	}

	public function filter()
	{
		$months = Input::get('months');
		$depo_ids = Input::get('depos');
		$parent_region_ids = Input::get('parent_regions');
		$region_ids = Input::get('regions');
		$area_ids = Input::get('areas');
		$outlet_type_ids = Input::get('outlet_types');
		$product_ids = Input::get('products');
		$product_taste_ids = Input::get('product_tastes');
		$sale_ids = Input::get('sales');
		$outlet_type_detail_ids = Input::get('outlet_type_details');
		$outlet_group_ids = Input::get('outlet_groups');
		$report_type = Input::get('report_type');
		$report_by = Input::get('report_by');
		$year = Input::get('year');
		// dd($report_type);

		// dd($depos);
		$years = Sellout::getYear();

		$month_lists = \Helper::MonthFilter($months);

		/*Filter parameters*/
		$parent_regions = ParentRegion::all();

		$regions = Region::all();

		$depos = Depo::all();

		$areas = Area::all();

		$distributors = Distributor::all();

		$outlet_types = OutletType::all();

		$products = Product::all();

		$sales = Sale::all();

		$outlet_groups = OutletGroup::all();

		$outlet_type_details = OutletTypeDetail::all();

		$product_tastes = ProductTaste::groupBy('name')->get();
		/*--end--*/

		$filters = [
			'year'=> $year,
			'parent_region_ids' => $parent_region_ids,
			'region_ids' => $region_ids,
			'depo_ids' => $depo_ids, 
			'area_ids' => $area_ids, 
			'outlet_type_ids' => $outlet_type_ids, 
			'product_ids' => $product_ids, 
			'product_taste_ids' => $product_taste_ids,
			'outlet_type_detail_ids' => $outlet_type_detail_ids,
			'outlet_group_ids' => $outlet_group_ids,
			'sale_ids' => $sale_ids
			];

		$report_sellout = SellOut::getSelloutFiltered($report_by, $months, $filters);

		$chart = 0;
		if($report_type != 'table' && !empty($report_type))
		{
			if($report_type != 'pie')
			{
				$chart_filter = Sellout::chartFilter($report_sellout, $month_lists, $report_type);
				$chart = $chart_filter;	
			}
			else
			{
				$pie_chart = Sellout::pieChartData($month_lists, $report_sellout);
				$chart = $pie_chart;
			}
		}

		return view('report.sellout.index')
				->with('data', $report_sellout)
				->with('chart', $chart)
				->with('months', $month_lists)
				->with('years', $years)
				->with('parent_regions', $parent_regions)
				->with('regions', $regions)
				->with('depos', $depos)
				->with('areas', $areas)
				->with('distributors', $distributors)
				->with('outlet_types', $outlet_types)
				->with('products', $products)
				->with('sales', $sales)
				->with('outlet_groups', $outlet_groups)
				->with('outlet_type_details', $outlet_type_details)
				->with('product_tastes', $product_tastes);
	}
}
