<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DataFile;
use App\Distributor;
use App\Depo;
use App\Region;
use App\ParentRegion;
use App\OutletType;
use App\Taste;
use App\Product;
use App\ProductTaste;
use App\SellIn;
use App\Stock;
use App\Area;
use Input;

use Illuminate\Http\Request;

class SellinController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('sellin.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$path = 'uploads/Sellin';

		$data_file = DataFile::uploadFile(Input::file('excel'), $path);

		// $read_excels = DataFile::readExcel('uploads/sell in feb 15 Test.xls', 'L');
		$read_excels = DataFile::readExcel($data_file->path, 'L');

		$field = [
					'TOTAL_ROW',
					'ROW',
					'NAME',
					'DEPO',
					'ID',
					'DATE',
					'QTY',
					'CODE',
					'AMOUNT',
					'TAX',
					'STATUS',
					'UNIT',
					'PRODUCT NAME',
					'OUTLET TYPE'
				];

		$check_format = \Helper::CheckFormat($read_excels[2], $field);

		if($check_format == FALSE)
		{
			// DataFile::deleteFile($data_file->id);	
			return response()->json('Format did not match', 400);
		}

		foreach($read_excels as $excel)
		{
			$distributor = $excel['name'];

			if(strpos($excel['name'], '-'))
			{
				$arr_col1 = explode('-', $excel['name']);
				$distributor =  $arr_col1[0];
			}
			
			$distributor_id = Distributor::ChecknInsert('name', $distributor);

			$depo = explode('-', $excel['depo']);
			$depo_id = Depo::rowExist('name', ucwords(strtolower($depo[1])));

			if($depo_id == FALSE)
			{
				$warning_message[] = "This Depo ". ucwords(strtolower($depo[1])) ." is not in database on row ".$excel['row'];
				continue;
			}

			$outlet_type = OutletType::rowExist('name', $excel['outlet type']);
			if($outlet_type == FALSE)
			{
				$warning_message[] = "This Outlet type ". $excel['outlet type'] ." is not in database on row ".$excel['row'];
				continue;
			}

			/*Check Gizo and Tama*/
			$check_spesial = \Helper::CheckSpecialProduct($excel['product name']);
			$check_spesial != FALSE ? $product_taste_name = $check_spesial : $product_taste_name = $excel['product name'];
			/*--end--*/
			
			$product_taste = ProductTaste::outletTypeExist($outlet_type, 'name', $product_taste_name);
			if($product_taste == FALSE)
			{
				$warning_message[] = "This Product taste ". $product_taste_name ." is not in database on row ".$excel['row'];
				continue;
			}
			
			$var_sellin = [
				'depo_id' => $depo_id,
				'outlet_type_id' => $outlet_type,
				'order_id' => \Helper::StringFormat($excel['id']),
				'product_taste_id' => $product_taste['id'],
				'sellin_date' => \Helper::StringFormat($excel['date']),
				'qty_ctn' => $excel['qty'],
				'amount' => $excel['amount']
			];

			$check_sellin = Sellin::checkDouble($var_sellin);

			if($check_sellin == TRUE)
			{
				$sellin = Sellin::create([
					'distributor_id' => $distributor_id,
					'depo_id' => $depo_id,
					'outlet_type_id' => $outlet_type,
					'product_taste_id' => $product_taste['id'],
					'order_id' => $excel['id'],
					'sellin_date' => $excel['date'],
					'qty_ctn' => $excel['qty'],
					'tax' => $excel['tax'],
					'status' => $excel['status'],
					'unit' => $excel['unit'],
					'amount' => $excel['amount']
				]);

				$check_stock = Stock::rowExist($depo_id, 'product_taste_id', $product_taste['id']);

				if($check_stock != "FALSE")
				{
					$stock = Stock::where('product_taste_id', '=', $product_taste['id'])
									->where('depo_id', '=', $depo_id);

					$stock_data = $stock->first();

					$stock_adj = \App\StockAdjustment::getTotalAdj($stock_data->id);

					$total_stock = $check_stock['stock'] + $excel['qty'];
					$total_up1 = $total_stock - $stock_adj['up2'] - $stock_adj['up3'];

					$stock->update(['stock' => $total_stock, 'up1' => $total_up1]);
				}
				else
				{
					Stock::create([
						'product_taste_id' => $product_taste['id'],
						'depo_id' => $depo_id,
						'stock' => $excel['qty'],
						'up1' => $excel['qty']
					]);
				}	
			}
			else
			{
				$warning_message[] = "There is double data in row ".$excel['row'];
			}
		}

		return response()->json(!empty($warning_message) ? $warning_message : "Success");
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Show report.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function report()
	{
		$month_lists = \Helper::MonthFilter();

		$report_sellin = SellIn::getFiltered();

		$years = SellIn::getYear();

		/* Filter */

		$parent_regions = ParentRegion::all();

		$regions = Region::all();

		$depos = Depo::all();

		$areas = Area::getAreaByHandler();

		$distributors = Distributor::all();

		$outlet_types = OutletType::all();

		$products = Product::all();

		$product_tastes = ProductTaste::groupBy('name')->get();

		/* --END-- */
		
		return view('report.sellin.index')
				->with('data', $report_sellin)
				->with('months', $month_lists)
				->with('years', $years)
				->with('parent_regions', $parent_regions)
				->with('regions', $regions)
				->with('depos', $depos)
				->with('areas', $areas)
				->with('distributors', $distributors)
				->with('outlet_types', $outlet_types)
				->with('products', $products)
				->with('product_tastes', $product_tastes);
	}

	public function filter()
	{
		$months = Input::get('months');
		$parent_region_ids = Input::get('parent_regions');
		$region_ids = Input::get('regions');
		$depo_ids = Input::get('depos');
		$area_ids = Input::get('areas');
		$outlet_type_ids = Input::get('outlet_types');
		$product_ids = Input::get('products');
		$product_taste_ids = Input::get('product_tastes');
		$report_type = Input::get('report_type');
		$report_by = Input::get('report_by');
		$year = Input::get('year');
		// dd($depos);
		$years = SellIn::getYear();

		$month_lists = \Helper::MonthFilter($months);

		/*Filter parameters*/
		$parent_regions = ParentRegion::all();

		$regions = Region::all();

		$depos = Depo::all();

		$areas = Area::getAreaByHandler();

		$distributors = Distributor::all();

		$outlet_types = OutletType::all();

		$products = Product::all();

		$product_tastes = ProductTaste::groupBy('name')->get();
		/*--end--*/

		$filters = [
			'year' => $year,
			'parent_region_ids' => $parent_region_ids,
			'region_ids' => $region_ids,
			'depo_ids' => $depo_ids, 
			'area_ids' => $area_ids, 
			'outlet_type_ids' => $outlet_type_ids, 
			'product_ids' => $product_ids, 
			'product_taste_ids' => $product_taste_ids
			];

		$report_sellin = SellIn::getFiltered($report_by, $months, $filters);
		// dd($report_sellin);
		$chart = 0;
		if($report_type != 'table' && !empty($report_type))
		{
			if($report_type != 'pie')
			{
				$chart_filter = SellIn::chartFilter($report_sellin, $month_lists, $report_type);
				$chart = $chart_filter;	
			}
			else
			{
				$pie_chart = Sellin::pieChartData($month_lists, $report_sellin);
				$chart = $pie_chart;
			}
			// echo "halo";
		}

		return view('report.sellin.index')
				->with('data', $report_sellin)
				->with('chart', $chart)
				->with('months', $month_lists)
				->with('years', $years)
				->with('parent_regions', $parent_regions)
				->with('regions', $regions)
				->with('depos', $depos)
				->with('areas', $areas)
				->with('distributors', $distributors)
				->with('outlet_types', $outlet_types)
				->with('products', $products)
				->with('product_tastes', $product_tastes);
	}	

}
