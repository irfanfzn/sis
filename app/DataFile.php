<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DataFile extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'data_files';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'mime', 'size', 'path'];

	static function validateFile($file)
	{
		$rules = [$file => 'mimes:xls,xlsx'];

		$validator = Validator::make(Input::all(), $rules);

		if($validator->passes())
		{
			return TRUE;
		}
		else
		{
			return $validator->errors();
		}
	}

	public static function uploadFile($file, $path)
	{
		$destination = public_path().'/'.$path;

		$mime = $file->getClientOriginalExtension();
		$size = $file->getSize();
		$filename = substr($file->getClientOriginalName(), 0, -5).'-'.time().'.'.$file->getClientOriginalExtension();
		$upload = $file->move($destination, $filename);

		$path = $path. '/'. $filename;

		if($upload)
		{
			$data = ['name' => $filename, 'mime' => $mime, 'size' => $size, 'path' => $path];
			$insert_data = DataFile::create($data);

			return $insert_data;
		}
	}

	public static function deleteFile($id)
	{
		$data_file = DataFile::where('id', '=', $id)->first();

		if(file_exists($data_file->path))
		{
			unlink($data_file->path);
			$delete = DataFile::destroy($id);
		}

		return "Deleted";
	}

	public static function readExcel($inputFileName, $highest_column)
	{
		$inputFileName = $inputFileName;

		try
		{
			$inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
			$objReader = \PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		}
		catch(Exception $e)
		{
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		// Set variable data
		$data = array();
		$data_header = array();

		$objWorksheet = $objPHPExcel->getActiveSheet();

		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $highest_column;
		$highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn);

		// Reformat Array
		for($row = 1; $row <= $highestRow; ++$row){
			for($col = 0; $col <= $highestColumnIndex; ++$col){
				if ($row == 1)
				{
					/* Get value without formating date */
					$dataval = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();

					$first_column = strtolower($dataval);
					$data_header[$col] = strtolower($dataval);
				}
				else
				{
					/* Formating date*/
					if(\PHPExcel_Shared_Date::isDateTime($objWorksheet->getCellByColumnAndRow($col, $row)))
					{
						$InvDate = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
						$dataval = date('Y-m-d', \PHPExcel_Shared_Date::ExcelToPHP($InvDate));
					}
					else
					{
						$dataval = $objWorksheet->getCellByColumnAndRow($col, $row)->getCalculatedValue();
					}

					$percentage = ($row / $highestRow) * 100;
					$percentage = round($percentage, 0);
					$dataval = preg_replace('/[^A-Za-z0-9\-\s?\/#$%^&*()+=\-\[\];,.:<>|]/', '', $dataval);
					$data[$row]['total_row'] = $highestRow;
					$data[$row]['row'] = $row;
					$data[$row][$data_header[$col]] = $dataval;
				}
			}
			// Break
			if (empty($data[$row]) && $row != 1) {
				break;
			}
		}
		return $data;
	}

}
