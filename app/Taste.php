<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Taste extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tastes';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['code', 'name'];

	/*
	Custom function
	*/
	public static function rowExist($field, $param)
	{
		$taste = Taste::where($field, '=', $param)->first();

		if(!empty($taste))
		{
			return $taste->id;
		}
		else
		{
			return FALSE;
		}
	}

}
