@extends('app')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Sell Out report</li>
		</ol>
		<div class="page-header">
			<h2>Sell Out report</h2>
		</div>	
		<!-- <ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="#">TAB 1</a></li>
		  	<li role="presentation"><a href="#">TAB 2</a></li>
		</ul> -->
		<!-- <div id="create-page" class="well bs-component"> -->
			{!! Form::open(['url' => 'report/filter', 'class' => 'form-horizontal form-custom form-report', 'id' => 'search-form']) !!}
			<fieldset>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('field', 'Report type : ') !!}<br>
						<select name='report_type' class='selectpicker' data-live-search="true" id="field">
							<option value="">Choose field</option>
							<option value="table" {{ \Helper::GetInput(Input::get('report_type'), 'table') }} >Table</option>
							<option value="line" {{ \Helper::GetInput(Input::get('report_type'), 'line') }} >Line chart</option>
							<option value="column" {{ \Helper::GetInput(Input::get('report_type'), 'column') }} >Bar chart</option>
							<option value="pie" {{ \Helper::GetInput(Input::get('report_type'), 'pie') }} >Pie chart</option>
						</select>
					</div>

					<div class="form-group">
						{!! Form::label('field', 'Parent region : ') !!}<br>
						<select class="lists" multiple="multiple" name="parent_regions[]">
							@foreach($parent_regions as $parent_region)
								<option value="{!! $parent_region->id !!}"
								{{ \Helper::GetInput(Input::get('parent_regions'), $parent_region->id) }}
								>{!! $parent_region->name !!}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
					{!! Form::label('parameter', 'Depo : ') !!}<br>
						<select class="lists" multiple="multiple" name="depos[]">
							@foreach($depos as $depo)
						  	<option value="{{ $depo->id }}" 
						  	{{ \Helper::GetInput(Input::get('depos'), $depo->id) }}
						  	>{{ $depo->name }}</option>
						  	@endforeach
						</select>
						<!-- <button type="submit" class="btn btn-primary">Search</button> -->
					</div>
					
					<div class="form-group">
						{!! Form::label('field', 'Product : ') !!}<br>
						<select class="lists" multiple="multiple" name="products[]">
							@foreach($products as $product)
								<option value="{!! $product->id !!}" 
								{{ \Helper::GetInput(Input::get('products'), $product->id) }}
								>{!! $product->name !!}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						{!! Form::label('field', 'Outlet group : ') !!}<br>
						<select class="lists" multiple="multiple" name="outlet_groups[]">
							@foreach($outlet_groups as $outlet_group)
								<option value="{!! $outlet_group->id !!}" 
								{{ \Helper::GetInput(Input::get('outlet_groups'), $outlet_group->id) }}
								>{!! $outlet_group->name !!}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary">Search</button>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('field', 'Report By : ') !!}<br>
						<select name='report_by' class='selectpicker' data-live-search="true" id="field">
							<option value="areas" {{ \Helper::GetInput(Input::get('report_by'), 'areas') }} >Area</option>
							<option value="parent_regions" {{ \Helper::GetInput(Input::get('report_by'), 'parent_regions') }} >Parent region</option>
							<option value="regions" {{ \Helper::GetInput(Input::get('report_by'), 'regions') }} >Region</option>
							<option value="depos" {{ \Helper::GetInput(Input::get('report_by'), 'depos') }} >Depo</option>
							<option value="outlet_types" {{ \Helper::GetInput(Input::get('report_by'), 'outlet_types') }} >Outlet Type</option>
							<option value="distributors" {{ \Helper::GetInput(Input::get('report_by'), 'distributor') }} >Distributor</option>
							<option value="products.name" {{ \Helper::GetInput(Input::get('report_by'), 'products') }} >Product</option>
							<option value="product_tastes" {{ \Helper::GetInput(Input::get('report_by'), 'product_tastes') }} >Product Tase</option>
							<option value="outlet_groups" {{ \Helper::GetInput(Input::get('report_by'), 'outlet_groups') }} >Outlet Group</option>
							<option value="outlet_type_details" {{ \Helper::GetInput(Input::get('report_by'), 'outlet_type_details') }} >Jenis Outlet</option>
						</select>
					</div>

					<div class="form-group">
						{!! Form::label('field', 'Region : ') !!}<br>
						<select class="lists" multiple="multiple" name="regions[]">
							@foreach($regions as $region)
								<option value="{!! $region->id !!}"
								{{ \Helper::GetInput(Input::get('regions'), $region->id) }}
								>{!! $region->name !!}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						{!! Form::label('field', 'Outlet type : ') !!}<br>
						<select class="lists" multiple="multiple" name="outlet_types[]">
							@foreach($outlet_types as $outlet_type)
								<option value="{!! $outlet_type->id !!}" 
									{{ \Helper::GetInput(Input::get('outlet_types'), $outlet_type->id) }}
									>
									{!! $outlet_type->name !!}
								</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						{!! Form::label('field', 'Product taste : ') !!}<br>
						<select class="lists" multiple="multiple" name="product_tastes[]">
							@foreach($product_tastes as $product_taste)
								<option value="{!! $product_taste->name !!}"
								{{ \Helper::GetInput(Input::get('product_tastes'), $product_taste->name) }}
								>{!! $product_taste->name !!}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						{!! Form::label('field', 'Jenis outlet : ') !!}<br>
						<select class="lists" multiple="multiple" name="outlet_type_details[]">
							@foreach($outlet_type_details as $outlet_type_detail)
								<option value="{!! $outlet_type_detail->name !!}"
								{{ \Helper::GetInput(Input::get('outlet_type_details'), $outlet_type_detail->name) }}
								>{!! $outlet_type_detail->name !!}</option>
							@endforeach
						</select>
					</div>

				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('field', 'Year : ') !!}<br>
						<select name='year' class='selectpicker' data-live-search="true" id="field">
							<option value="">Choose field</option>
							@foreach($years as $year)
								<option value="{!! $year->sell_year !!}" 
								{!! \Helper::GetInput(Input::get('year'), $year->sell_year) !!}
								>{!! $year->sell_year !!}</option>
							@endforeach
						</select>
					</div>
					
					<div class="form-group">
						{!! Form::label('parameter', 'Month : ') !!}<br>
						<select class="lists" multiple="multiple" name="months[]">
							
						  	<option value="1" {{ \Helper::GetInput(Input::get('months'), '1') }}>January</option>
						  	<option value="2" {{ \Helper::GetInput(Input::get('months'), '2') }}>February</option>
						  	<option value="3" {{ \Helper::GetInput(Input::get('months'), '3') }}>March</option>
						  	<option value="4" {{ \Helper::GetInput(Input::get('months'), '4') }}>April</option>
						  	<option value="5" {{ \Helper::GetInput(Input::get('months'), '5') }}>May</option>
						  	<option value="6" {{ \Helper::GetInput(Input::get('months'), '6') }}>Juny</option>
						  	<option value="7" {{ \Helper::GetInput(Input::get('months'), '7') }}>July</option>
						  	<option value="8" {{ \Helper::GetInput(Input::get('months'), '8') }}>August</option>
						  	<option value="9" {{ \Helper::GetInput(Input::get('months'), '9') }}>September</option>
						  	<option value="10" {{ \Helper::GetInput(Input::get('months'), '10') }}>October</option>
						  	<option value="11" {{ \Helper::GetInput(Input::get('months'), '11') }}>November</option>
						  	<option value="12" {{ \Helper::GetInput(Input::get('months'), '12') }}>December</option>
						  	
						</select>
					</div>

					<div class="form-group">
						{!! Form::label('field', 'Area : ') !!}<br>
						<select class="lists" multiple="multiple" name="areas[]">
							@foreach($areas as $area)
								<option value="{!! $area->id !!}"
								{{ \Helper::GetInput(Input::get('areas'), $area->id) }}
								>{!! $area->name !!}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						{!! Form::label('field', 'Distributor : ') !!}<br>
						<select class="lists" multiple="multiple" name="distributors[]">
							@foreach($distributors as $distributor)
								<option value="{!! $distributor->id !!}"
								{{ \Helper::GetInput(Input::get('distributors'), $distributor->id) }}
								>{!! $distributor->name !!}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						{!! Form::label('field', 'Sales : ') !!}<br>
						<select class="lists" multiple="multiple" name="sales[]">
							@foreach($sales as $sale)
								<option value="{!! $sale->id !!}"
								{{ \Helper::GetInput(Input::get('sales'), $sale->id) }}
								>{!! $sale->name !!}</option>
							@endforeach
						</select>
					</div>

				</div>
			</fieldset>
			{!! Form::close() !!}
		<!-- </div> -->
		@if(Input::get('report_type') != 'table' && !empty(Input::get('report_type')))

			@if(Input::get('report_type') != 'pie')

			<div class='chart-div' style="height:500px">
				<!-- <div id="chartContainer" style="height: 300px; width: 100%;"></div> -->
				@chart($chart)
			</div>

			@else

			<div class='chart-div' style="height:450px">
				
				@foreach($months as $key => $month)
				
				@if(!empty($chart[$key]))
				<div @if(count($chart) != 1) class="col-xs-6" @endif>
					<div id="{!!$chart[$key]->getID()!!}" {!! \HTML::attributes($chart[$key]->getAttributesArray()) !!}></div>

				</div>
				@endif

				@endforeach
				
				<script type="text/javascript">
				
				window.onload = function ()
				{
				
					@foreach($months as $key => $month)
						@if(!empty($chart[$key]))
						var {!!$chart[$key]->getID()!!} = new CanvasJS.Chart("{!!$chart[$key]->getID()!!}",{!!$chart[$key]->getChart()!!});
							{!!$chart[$key]->getID()!!}.render();
						@endif
					@endforeach
				}

				</script>

			</div>

			@endif

		@endif

		<div class="table-responsive table-report" id="list_page" style="display: block;">
			<table class="table table-striped table-hover table-bordered ">
			  	<thead>
				    <tr>
				      <th rowspan='2'>AREA</th>
				      	@foreach($months as $month)
						<th colspan='4'>{!! $month !!}</th>
				      	@endforeach
				    </tr>
				    <tr>
			    		@foreach($months as $month)
						<th>CTN</th>
						<th>Return</th>
						<th>Omzet</th>
						<th>Net</th>
						@endforeach
				    </tr>
			  	</thead>
			  	<tbody>
			  	<!-- FOREACH -->
	  				@foreach($data as $row)
				    <tr>
						<td width="100">{!! $row['area_name'] !!}</td>
			  			@foreach($months as $a => $month)
							<td>{!! number_format($row["ctn_$a"], 2) !!}</td>
							<td>{!! number_format($row["return_$a"], 2) !!}</td>
							<td>{!! $row["omzet_$a"] != 0 ? Helper::Currency($row["omzet_$a"]) : 0 !!}</td>
							<td>{!! $row["net_$a"] != 0 ? Helper::Currency($row["net_$a"]) : 0 !!}</td>
			    		@endforeach
				    </tr>
			    	@endforeach
			    <tfoot>
			    	<tr>
				  		<td>Total</td>
				  		@foreach($months as $a => $month)
				  			<?php 
				  			$total_qty = 0; 
				  			$total_return = 0; 
				  			$total_omzet = 0;
				  			$total_net = 0;
				  			?>
				  			@foreach($data as $row)
				  				<?php 
				  				$total_qty += $row["ctn_$a"];
				  				$total_return += $row["return_$a"];
				  				$total_omzet += $row["omzet_$a"];
				  				$total_net += $row["net_$a"];
				  				?>
				  			@endforeach
				  			<td>{!! number_format($total_qty, 2) !!}</td>
				  			<td>{!! number_format($total_return,2 ) !!}</td>
				  			<td>{!! $total_omzet != 0 ? Helper::Currency($total_omzet) : 0 !!}</td>
				  			<td>{!! $total_net != 0 ? Helper::Currency($total_net) : 0 !!}</td>
				  		@endforeach
				  	</tr>
				</tfoot>
				<!-- END FOREACH -->
			  </tbody>
			</table>
			<!-- PAGING 	 -->
		</div>
	</div>
</div>

@endsection