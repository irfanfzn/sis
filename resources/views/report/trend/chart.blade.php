@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Trend</li>
		</ol>
		<div class="page-header">
			<h2>Trend</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation"><a href="/report/trend">Target list</a></li>
		  	<li role="presentation" class="active"><a href="#">Trend chart</a></li>
		</ul>
		<!-- <ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="#">TAB 1</a></li>
		  	<li role="presentation"><a href="#">TAB 2</a></li>
		</ul> -->


		{!! Form::open(['url' => 'report/trend/chart', 'class' => 'form-horizontal form-custom form-report', 'id' => 'search-form']) !!}
			<fieldset>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('field', 'Report type : ') !!}<br>
						<select name='report_type' class='selectpicker' data-live-search="true" id="field">
							<option value="">Choose field</option>
							<option value="line" {{ \Helper::GetInput(Input::get('report_type'), 'line') }} >Line chart</option>
							<option value="column" {{ \Helper::GetInput(Input::get('report_type'), 'column') }} >Bar chart</option>
							<!-- <option value="pie" {{ \Helper::GetInput(Input::get('report_type'), 'pie') }} >Pie chart</option> -->
						</select>
					</div>

					<div class="form-group">
						{!! Form::label('field', 'Area : ') !!}<br>
						<select class="lists" multiple="multiple" name="areas[]">
							@foreach($areas as $area)
								<option value="{!! $area->id !!}"
								{{ \Helper::GetInput(Input::get('areas'), $area->id) }}
								>{!! $area->name !!}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary">Search</button>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('field', 'Product : ') !!}<br>
						<select class="lists" multiple="multiple" name="products[]">
							@foreach($products as $product)
								<option value="{!! $product->id !!}" 
								{{ \Helper::GetInput(Input::get('products'), $product->id) }}
								>{!! $product->name !!}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						{!! Form::label('field', 'Outlet type : ') !!}<br>
						<select class="lists" multiple="multiple" name="outlet_types[]">
							@foreach($outlet_types as $outlet_type)
								<option value="{!! $outlet_type->id !!}" 
									{{ \Helper::GetInput(Input::get('outlet_types'), $outlet_type->id) }}
									>
									{!! $outlet_type->name !!}
								</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('field', 'Outlet group : ') !!}<br>
						<select class="lists" multiple="multiple" name="outlet_groups[]">
							@foreach($outlet_groups as $outlet_group)
								<option value="{!! $outlet_group->id !!}" 
								{{ \Helper::GetInput(Input::get('outlet_groups'), $outlet_group->id) }}
								>{!! $outlet_group->name !!}</option>
							@endforeach
						</select>
					</div>
				</div>
			</fieldset>
		{!! Form::close() !!}
		
		<div class='chart-div'>
			<!-- <div id="chartContainer" style="height: 300px; width: 100%;"></div> -->
			@chart($chart)
		</div>
	</div>
</div>

@endsection