@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Trend</li>
		</ol>
		<div class="page-header">
			<h2>Trend</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="#">Target list</a></li>
		  	<li role="presentation"><a href="/report/trend/chart">Trend chart</a></li>
		</ul>
		{!! Session::get('message') ? \Helper::ShowMessage(Session::get('message'), 1) : '' !!}

		{!! Form::open(['class' => 'form-horizontal form-custom form-report', 'id' => 'search-form']) !!}
			<fieldset>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('field', 'Report type : ') !!}<br>
						<select name='report_type' class='selectpicker' data-live-search="true" id="field">
							<option value="">Choose field</option>
							<!-- <option value="table" {{ \Helper::GetInput(Input::get('report_type'), 'table') }} >Table</option>
							<option value="line" {{ \Helper::GetInput(Input::get('report_type'), 'line') }} >Line chart</option>
							<option value="column" {{ \Helper::GetInput(Input::get('report_type'), 'column') }} >Bar chart</option>
							<option value="pie" {{ \Helper::GetInput(Input::get('report_type'), 'pie') }} >Pie chart</option> -->
						</select>
					</div>

					<div class="form-group">
						{!! Form::label('field', 'Area : ') !!}<br>
						<select class="lists" multiple="multiple" name="areas[]">
							@foreach($areas as $area)
								<option value="{!! $area->id !!}"
								{{ \Helper::GetInput(Input::get('areas'), $area->id) }}
								>{!! $area->name !!}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary">Search</button>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('parameter', 'Month : ') !!}<br>
						<select class="lists" multiple="multiple" name="months[]">
							
						  	<option value="1" {{ \Helper::GetInput(Input::get('months'), '1') }}>January</option>
						  	<option value="2" {{ \Helper::GetInput(Input::get('months'), '2') }}>February</option>
						  	<option value="3" {{ \Helper::GetInput(Input::get('months'), '3') }}>March</option>
						  	<option value="4" {{ \Helper::GetInput(Input::get('months'), '4') }}>April</option>
						  	<option value="5" {{ \Helper::GetInput(Input::get('months'), '5') }}>May</option>
						  	<option value="6" {{ \Helper::GetInput(Input::get('months'), '6') }}>June</option>
						  	<option value="7" {{ \Helper::GetInput(Input::get('months'), '7') }}>July</option>
						  	<option value="8" {{ \Helper::GetInput(Input::get('months'), '8') }}>August</option>
						  	<option value="9" {{ \Helper::GetInput(Input::get('months'), '9') }}>September</option>
						  	<option value="10" {{ \Helper::GetInput(Input::get('months'), '10') }}>October</option>
						  	<option value="11" {{ \Helper::GetInput(Input::get('months'), '11') }}>November</option>
						  	<option value="12" {{ \Helper::GetInput(Input::get('months'), '12') }}>December</option>
						  	
						</select>
					</div>

					<div class="form-group">
						{!! Form::label('field', 'Product : ') !!}<br>
						<select class="lists" multiple="multiple" name="products[]">
							@foreach($products as $product)
								<option value="{!! $product->id !!}" 
								{{ \Helper::GetInput(Input::get('products'), $product->id) }}
								>{!! $product->name !!}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('field', 'Outlet group : ') !!}<br>
						<select class="lists" multiple="multiple" name="outlet_groups[]">
							@foreach($outlet_groups as $outlet_group)
								<option value="{!! $outlet_group->id !!}" 
								{{ \Helper::GetInput(Input::get('outlet_groups'), $outlet_group->id) }}
								>{!! $outlet_group->name !!}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						{!! Form::label('field', 'Outlet type : ') !!}<br>
						<select class="lists" multiple="multiple" name="outlet_types[]">
							@foreach($outlet_types as $outlet_type)
								<option value="{!! $outlet_type->id !!}" 
									{{ \Helper::GetInput(Input::get('outlet_types'), $outlet_type->id) }}
									>
									{!! $outlet_type->name !!}
								</option>
							@endforeach
						</select>
					</div>
				</div>
			</fieldset>
			<!-- <div class="form-group">
				{!! Form::label('field', 'Field : ') !!}
				<select name='field' class='selectpicker' data-live-search="true" id="field">
					<option value="">Choose field</option>
				</select>
			</div>
			<div class="form-group">
				{!! Form::label('parameter', 'Parameter : ') !!}
				<select name='parameter' class='selectpicker' data-live-search="true" id="content">
					<option value="">Parameter</option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Search</button>
			</div> -->
		{!! Form::close() !!}
		
		<div class="table-responsive table-report" id="list_page" style="display: block;">
			<table class="table table-striped table-hover table-bordered ">
			  	<thead>
				    <tr>
					    <th rowspan='2'>AREA</th>
					    @foreach($months as $month)
					    <th>{!! $month !!}</th>
					    @endforeach
				    </tr>
			  	</thead>
			  	<tbody>
			  	<!-- FOREACH -->
			  	@foreach($data as $row)
				    <tr>
						<td width="100">{!! $row['area_name'] !!}</td>
			  			@foreach($months as $a => $month)
							<td>{!! $row["target_$a"] != 0 ? Helper::Currency($row["target_$a"]) : 0 !!}</td>
			    		@endforeach
				    </tr>
		    	@endforeach
				<!-- END FOREACH -->
			  </tbody>
			</table>
			<!-- PAGING 	 -->
		</div>
	</div>
</div>

@endsection