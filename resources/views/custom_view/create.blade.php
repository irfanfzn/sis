@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<!-- BREADCRUMB -->
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active"><!-- FILL THIS --></li>
		</ol>
		<!-- END -->
		<!-- HEADER -->
		<div class="page-header">
			<h2>Product</h2>
		</div>
		<!-- END -->	
		<!-- NAV TAB -->
		<ul class="nav nav-tabs">
		  	<li role="presentation"><a href="/product">TAB 1</a></li>
		  	<li role="presentation" class="active"><a href="#">TAB 2</a></li>
		</ul>
		<!-- END -->
		<div id="create-page" class="well bs-component">
			{!! Session::get('message') ? \Helper::ShowMessage(Session::get('message'), 1) : '' !!}
			
			{!! Form::open(['class' => 'form-horizontal', 'files' => true, 'id' => 'createForm']) !!}
				<fieldset>
					<div class="form-group">
				      	{!! Form::label('brand_id', 'Brand : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	<select name='brand_id' class='selectpicker' data-live-search="true" id="content">
				        		<option value="">Choose brand</option>
							</select>
				      	</div>
				    </div>

				    <div class="form-group">
				      	{!! Form::label('code', 'Code : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-sm-2">
				        	{!! Form::text('code', null, ['id' => 'product-code', 'class' => 'form-control', 'required' => 'required']) !!}
				      	</div>
				    </div>

				    <div class="form-group">
				      	{!! Form::label('name', 'Product name : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	{!! Form::text('name', null, ['id' => 'product-name', 'class' => 'form-control', 'required' => 'required']) !!}
				      	</div>
				    </div>

				    <div class="form-group">
				    	<div class="col-md-9 col-md-offset-3">
				    		{!! Form::submit('Submit', ['class' => 'btn btn-primary', 'id' => 'outlet-import']) !!}
				    	</div>
				    </div>

				</fieldset>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endsection