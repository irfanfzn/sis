@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Location</li>
		</ol>
		<div class="page-header">
			<h2>Header</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="#">TAB 1</a></li>
		  	<li role="presentation"><a href="#">TAB 2</a></li>
		</ul>


		{!! Form::open(['url' => 'outlet/search', 'class' => 'form-inline form-custom', 'id' => 'search-form']) !!}
			<div class="form-group">
				{!! Form::label('field', 'Field : ') !!}
				<select name='field' class='selectpicker' data-live-search="true" id="field">
					<option value="">Choose field</option>
					<option value="">FILED 1</option>
					<option value="">FIELD 2</option>
					<option value="">FIELD 3</option>
				</select>
			</div>
			<div class="form-group">
				{!! Form::label('parameter', 'Parameter : ') !!}
				<select name='parameter' class='selectpicker' data-live-search="true" id="content">
					<option value="">Parameter</option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Search</button>
			</div>
		{!! Form::close() !!}
		
		<div class="table-responsive" id="list_page" style="display: block;">
			<table class="table table-striped table-hover table-bordered ">
			  	<thead>
				    <tr>
				      <th>id</th>
				      <th>HEADER 1</th>
				      <th>HEADER 2</th>
				      <th>HEADER 3</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  	<!-- FOREACH -->
				    <tr>
				      <td>Content 1</td>
				      <td>Content 2</td>
				      <td>Content 3</td>
				      <td>Content 4</td>
				    </tr>
				<!-- END FOREACH -->
			  </tbody>
			</table>
			<!-- PAGING 	 -->
		</div>
	</div>
</div>

@endsection