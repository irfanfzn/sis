@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Product</li>
		</ol>
		<div class="page-header">
			<h2>Product</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="#">Product list</a></li>
		  	<li role="presentation"><a href="/product/create">Create product</a></li>
		</ul>


		{!! Form::open(['url' => 'product/search', 'class' => 'form-inline form-custom', 'id' => 'search-form']) !!}
			<div class="form-group">
				<input type="hidden" name="field_url" value="/product/field">
				{!! Form::label('field', 'Field : ') !!}
				<select name='field' class='selectpicker' data-live-search="true" id="field">
					<option value="">Choose field</option>
					<option value="brands.name">Brand</option>
					<option value="code">Code</option>
					<option value="products.name">Product name</option>
				</select>
			</div>
			<div class="form-group">
				{!! Form::label('parameter', 'Parameter : ') !!}
				<select name='parameter' class='selectpicker' data-live-search="true" id="content">
					<option value="">Parameter</option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Search</button>
			</div>
		{!! Form::close() !!}
		
		<div class="table-responsive" id="list_page" style="display: block;">
			<table class="table table-striped table-hover table-bordered ">
			  	<thead>
				    <tr>
				      <th>id</th>
				      <th>Brand</th>
				      <th>Code</th>
				      <th>Product name</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  	@foreach($products as $product)
				    <tr>
				      <td>{{ $product->id }}</td>
				      <td>{{ $product->brand->name }}</td>
				      <td>{{ $product->code }}</td>
				      <td>{{ $product->name }}</td>
				    </tr>
				@endforeach
			  </tbody>
			</table>
			{!! $products->render() !!}
		</div>
	</div>
</div>

@endsection