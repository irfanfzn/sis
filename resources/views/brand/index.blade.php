@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Brand</li>
		</ol>
		<div class="page-header">
			<h2>Brand</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="#">Brand list</a></li>
		  	<li role="presentation"><a href="/brand/create">Create brand</a></li>
		</ul>


		{!! Form::open(['url' => 'brand/search', 'class' => 'form-inline form-custom', 'id' => 'search-form']) !!}
			<div class="form-group">
				<input type="hidden" name="field_url" value="/brand/field">
				{!! Form::label('field', 'Field : ') !!}
				<select name='field' class='selectpicker' data-live-search="true" id="field">
					<option value="">Choose field</option>
					<option value="name">Name</option>
					<option value="status">Status</option>
				</select>
			</div>
			<div class="form-group">
				{!! Form::label('parameter', 'Parameter : ') !!}
				<select name='parameter' class='selectpicker' data-live-search="true" id="content">
					<option value="">Parameter</option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Search</button>
			</div>
		{!! Form::close() !!}
		
		<div class="table-responsive" id="list_page" style="display: block;">
			<table class="table table-striped table-hover table-bordered ">
			  	<thead>
				    <tr>
				      <th>id</th>
				      <th>Name</th>
				      <th>Status</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  	@foreach($data as $brand)
				    <tr>
				      <td>{{ $brand->id }}</td>
				      <td>{{ $brand->name }}</td>
				      <td>{{ $brand->status }}</td>
				    </tr>
				@endforeach
			  </tbody>
			</table>
		</div>
	</div>
</div>

@endsection