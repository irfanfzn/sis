@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Outlet</li>
		</ol>
		<div class="page-header">
			<h2>Sell Out</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="#">Outlet list</a></li>
		  	<li role="presentation"><a href="/sellout/create">Import sell out</a></li>
		</ul>


		{!! Form::open(['url' => 'outlet/search', 'class' => 'form-inline form-custom', 'id' => 'search-form']) !!}
			<div class="form-group">
				{!! Form::label('field', 'Field : ') !!}
				<select name='field' class='selectpicker' data-live-search="true" id="field">
					<option value="">Choose field</option>
					<option value="code">Code</option>
					<option value="name">Outlet name</option>
					<option value="address">Address</option>
					<option value="group">group</option>
					<option value="type">type</option>
				</select>
			</div>
			<div class="form-group">
				{!! Form::label('parameter', 'Parameter : ') !!}
				<select name='parameter' class='selectpicker' data-live-search="true" id="content">
					<option value="">Parameter</option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Search</button>
			</div>
		{!! Form::close() !!}
		
		<div class="table-responsive" id="list_page" style="display: block;">
			<table class="table table-striped table-hover table-bordered ">
			  	<thead>
				    <tr>
				      <th>id</th>
				      <th>Code</th>
				      <th>Outlet name</th>
				      <th>Address</th>
				      <th>group</th>
				      <th>type</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  
				    <tr>
				      <td>1</td>
				      <td>content</td>
				      <td>content</td>
				      <td>content</td>
				      <td>content</td>
				      <td>content</td>
				    </tr>
				
			  </tbody>
			</table>
		</div>
	</div>
</div>

@endsection