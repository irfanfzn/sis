@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="page-header">
			<div class="panel panel-default">
				{!! Session::get('message') ? \Helper::ShowMessage(Session::get('message'), 1) : '' !!}
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					You are logged in!
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
