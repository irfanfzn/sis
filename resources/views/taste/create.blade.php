@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Taste varian</li>
		</ol>
		<div class="page-header">
			<h2>Taste varian</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation"><a href="/taste">Taste list</a></li>
		  	<li role="presentation" class="active"><a href="#">Create taste</a></li>
		</ul>
		<div id="create-page" class="well bs-component">
			{!! Session::get('message') ? \Helper::ShowMessage(Session::get('message'), 1) : '' !!}
			
			{!! Form::open(['class' => 'form-horizontal', 'files' => true, 'id' => 'createForm']) !!}
				<fieldset>
				    <div class="form-group">
				      	{!! Form::label('status', 'Code : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-sm-2">
				        	{!! Form::text('code', null, ['id' => 'taste-code', 'class' => 'form-control', 'required' => 'required']) !!}
				      	</div>
				    </div>

				    <div class="form-group">
				      	{!! Form::label('name', 'Name : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	{!! Form::text('name', null, ['id' => 'taste-name', 'class' => 'form-control', 'required' => 'required']) !!}
				      	</div>
				    </div>

				    <div class="form-group">
				    	<div class="col-md-9 col-md-offset-3">
				    		{!! Form::submit('Submit', ['class' => 'btn btn-primary', 'id' => 'outlet-import']) !!}
				    	</div>
				    </div>

				</fieldset>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="loading-progress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
	        <div class="modal-header">
		        <h4 class="modal-title" style="text-align:center">Importing data...</h4>
	      	</div>
      		<div class="modal-body">
				<div id="import-file">
      				<p class="col-md-offset-5">Importing file 
      				</p>
	      			<div class="progress progress-striped active">
						<div class="progress-bar" style="width: 100%"></div>
					</div>
				</div>

				<div id="error-message">
					<!-- AJAX ERROR APPEND HERE -->
				</div>

				<div id='success-message'>
					<!-- AJAX SUCCESS APPEND HERE -->
				</div>
      		</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    </div>
	    </div>
  	</div>
</div>
@endsection