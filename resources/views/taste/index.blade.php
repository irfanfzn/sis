@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Taste varian</li>
		</ol>
		<div class="page-header">
			<h2>Taste varian</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="#">Taste list</a></li>
		  	<li role="presentation"><a href="/taste/create">Create taste</a></li>
		</ul>
		{!! Form::open(['url' => 'outlet/search', 'class' => 'form-inline form-custom', 'id' => 'search-form']) !!}
			<div class="form-group">
				{!! Form::label('field', 'Field : ') !!}
				<select name='field' class='selectpicker' data-live-search="true" id="field">
					<option value="">Choose field</option>
					<option value="code">Code</option>
					<option value="name">Outlet name</option>
					<option value="address">Address</option>
					<option value="group">group</option>
					<option value="type">type</option>
				</select>
			</div>
			<div class="form-group">
				{!! Form::label('parameter', 'Parameter : ') !!}
				<select name='parameter' class='selectpicker' data-live-search="true" id="content">
					<option value="">Parameter</option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Search</button>
			</div>
		{!! Form::close() !!}
		
		<div class="table-responsive" id="list_page" style="display: block;">
			<table class="table table-striped table-hover table-bordered ">
			  	<thead>
				    <tr>
				      <th>id</th>
				      <th>Code</th>
				      <th>Taste name</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  	@foreach($tastes as $taste)
				    <tr>
				      <td>{{ $taste->id }}</td>
				      <td>{{ $taste->code }}</td>
				      <td>{{ $taste->name }}</td>
				    </tr>
				@endforeach
			  </tbody>
			</table>
		</div>
	</div>
</div>

@endsection