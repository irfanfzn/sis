@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Target</li>
		</ol>
		<div class="page-header">
			<h2>Best Estimate Target</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="#">BE list</a></li>
		  	<!-- <li role="presentation"><a href="/target/create">Create BE</a></li> -->
		  	<li role="presentation"><a href="/target/import">Import target</a></li>
		</ul>
		{!! Session::get('message') ? \Helper::ShowMessage(Session::get('message'), 1) : '' !!}

		{!! Form::open(['url' => 'outlet/search', 'class' => 'form-inline form-custom', 'id' => 'search-form']) !!}
			<div class="form-group">
				{!! Form::label('field', 'Field : ') !!}
				<select name='field' class='selectpicker' data-live-search="true" id="field">
					<option value="">Choose field</option>
				</select>
			</div>
			<div class="form-group">
				{!! Form::label('parameter', 'Parameter : ') !!}
				<select name='parameter' class='selectpicker' data-live-search="true" id="content">
					<option value="">Parameter</option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Search</button>
			</div>
		{!! Form::close() !!}
		
		<div class="table-responsive" id="list_page" style="display: block;">
			<table class="table table-striped table-hover table-bordered ">
			  	<thead>
				    <tr>
				      <th>id</th>
				      <th>Month</th>
				      <th>Year</th>
				      <th>Best Estimate</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  	<!-- FOREACH -->
			  	@foreach($data as $row)
				    <tr>
				      <td>{!! $row->id !!}</td>
				      <td>{!! $row->target_month !!}</td>
				      <td>{!! $row->target_year !!}</td>
				      <td>{!! Helper::Currency($row->base_estimate) !!}</td>
				    </tr>
				@endforeach
				<!-- END FOREACH -->
			  </tbody>
			</table>
			<!-- PAGING 	 -->
		</div>
	</div>
</div>

@endsection