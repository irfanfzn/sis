@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<!-- BREADCRUMB -->
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Target</li>
		</ol>
		<!-- END -->
		<!-- HEADER -->
		<div class="page-header">
			<h2>Best Estimate Target</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation"><a href="/target">BE list</a></li>
		  	<li role="presentation" class="active"><a href="#">Create BE</a></li>
		  	<li role="presentation"><a href="/target/import">Import actual target</a></li>
		</ul>
		<div id="create-page" class="well bs-component">
			{!! Session::get('message') ? \Helper::ShowMessage(Session::get('message'), 1) : '' !!}
			
			{!! \Helper::ErrorMessage($errors) !!}
			
			{!! Form::open(['class' => 'form-horizontal', 'id' => 'createForm']) !!}
				<fieldset>
					<div class="form-group">
				      	{!! Form::label('brand_id', 'Brand : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	<select name='month' class='selectpicker' data-live-search="true" id="field">
								<option value="">Choose month</option>
								<option value="January">January</option>
								<option value="February">February</option>
								<option value="March">March</option>
								<option value="April">April</option>
								<option value="May">May</option>
								<option value="June">June</option>
								<option value="July">July</option>
								<option value="August">August</option>
								<option value="September">September</option>
								<option value="October">October</option>
								<option value="November">November</option>
								<option value="December">December</option>
							</select>
				      	</div>
				    </div>

				    <div class="form-group">
				      	{!! Form::label('code', 'Year : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-sm-2">
				        	{!! Form::text('year', null, ['class' => 'form-control', 'required' => 'required']) !!}
				      	</div>
				    </div>

				    <div class="form-group">
				      	{!! Form::label('name', 'Best estimate : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	{!! Form::text('best_estimate', null, ['class' => 'form-control', 'required' => 'required']) !!}
				      	</div>
				    </div>

				    <div class="form-group">
				    	<div class="col-md-9 col-md-offset-3">
				    		{!! Form::submit('Submit', ['class' => 'btn btn-primary', 'id' => 'outlet-import']) !!}
				    	</div>
				    </div>

				</fieldset>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endsection