@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Target</li>
		</ol>
		<!-- END -->
		<!-- HEADER -->
		<div class="page-header">
			<h2>Best Estimate Target</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<!-- <li role="presentation"><a href="/target">BE list</a></li> -->
		  	<!-- <li role="presentation"><a href="/target/create">Import BE</a></li> -->
		  	<li role="presentation" class="active"><a href="#">Import target</a></li>
		</ul>
		<div id="create-page" class="well bs-component">			
			{!! \Helper::ErrorMessage($errors) !!}
			
			{!! Form::open(['class' => 'form-horizontal', 'files' => true, 'id' => 'uploadForm']) !!}
				<fieldset>

					<div class="form-group">
				      	{!! Form::label('field', 'Target type : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	<select name='target_type' class='selectpicker' data-live-search="true" id="field">
								<option value="">Choose type</option>
								<option value="actual">Actual</option>
								<option value="be">Best estimate</option>
								<!-- <option value="pie">Pie</option> -->
							</select>
				      	</div>
				    </div>

					<div class="form-group">
				      	{!! Form::label('code', 'Year : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-sm-2">
				        	{!! Form::text('year', null, ['class' => 'form-control', 'required' => 'required']) !!}
				      	</div>
				    </div>
				    
					<div class="form-group">
				      	{!! Form::label('excel', 'Excel file : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	{!! Form::file('excel', null, ['id' => 'excel']) !!}
				      	</div>
				    </div>
				    <div class="form-group">
				    	<div class="col-md-9 col-md-offset-3">
				    		{!! Form::submit('Import', ['class' => 'btn btn-primary', 'id' => 'outlet-import']) !!}
				    	</div>
				    </div>
				</fieldset>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="loading-progress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
	        <div class="modal-header">
		        <h4 class="modal-title" style="text-align:center">Importing data...</h4>
	      	</div>
      		<div class="modal-body">
				<div id="import-file">
      				<p class="col-md-offset-5">Importing file 
      				</p>
	      			<div class="progress progress-striped active">
						<div class="progress-bar" style="width: 100%"></div>
					</div>
				</div>

				<div id="error-message">
					<!-- AJAX ERROR APPEND HERE -->
				</div>

				<div id='success-message'>
					<!-- AJAX SUCCESS APPEND HERE -->
				</div>
      		</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary save-change">Save changes</button>
		    </div>
	    </div>
  	</div>
</div>
@endsection