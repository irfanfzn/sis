@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">User Management</li>
		</ol>
		<div class="page-header">
			<h2>User Management</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="#">User list</a></li>
		  	<li role="presentation"><a href="user/create">Create user</a></li>
		</ul>


		{!! Form::open(['url' => 'outlet/search', 'class' => 'form-inline form-custom', 'id' => 'search-form']) !!}
			<!-- <div class="form-group">
				{!! Form::label('field', 'Field : ') !!}
				<select name='field' class='selectpicker' data-live-search="true" id="field">
					<option value="">Choose field</option>
					<option value="">FILED 1</option>
					<option value="">FIELD 2</option>
					<option value="">FIELD 3</option>
				</select>
			</div>
			<div class="form-group">
				{!! Form::label('parameter', 'Parameter : ') !!}
				<select name='parameter' class='selectpicker' data-live-search="true" id="content">
					<option value="">Parameter</option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Search</button>
			</div> -->
		{!! Form::close() !!}
		
		<div class="table-responsive" id="list_page" style="display: block;">
			<table class="table table-striped table-hover table-bordered ">
			  	<thead>
				    <tr>
				      <th>id</th>
				      <th>Username</th>
				      <th>Role</th>
				      <th>Name</th>
				      <th>Email</th>
				      <th>Created at</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  	<!-- FOREACH -->
			  	@foreach($users as $user)
				    <tr>
				      <td>{{ $user->id }}</td>
				      <td>{{ $user->username }}</td>
				      <td>{{ $user->UserRole->name }}</td>
				      <td>{{ $user->name }}</td>
				      <td>{{ $user->email }}</td>
				      <td>{{ \Helper::DateFormat($user->created_at) }}</td>
				    </tr>
				@endforeach
				<!-- END FOREACH -->
			  </tbody>
			</table>
			<!-- PAGING 	 -->
		</div>
	</div>
</div>

@endsection