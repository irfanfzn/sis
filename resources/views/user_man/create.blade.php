@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">User Management</li>
		</ol>
		<div class="page-header">
			<h2>User Management</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation"><a href="/user">User list</a></li>
		  	<li role="presentation" class="active"><a href="#">Create user</a></li>
		</ul>
		
		<div id="create-page" class="well bs-component">
			{!! Session::get('message') ? \Helper::ShowMessage(Session::get('message'), 1) : '' !!}

			{!! \Helper::ErrorMessage($errors)!!}

			<form class="form-horizontal" role="form" method="POST" action="{{ url('/user/create') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="form-group">
			    	{!! Form::label('roles', 'Role ', ['class' => 'col-md-4 control-label']) !!}
			      	<div class="col-md-4">
			        	<select name='user_role_id' class='selectpicker role-reg' data-live-search="true" id="content">
			        		<option value="">Choose user role</option>
		        			@foreach($user_roles as $role)
			        		<option value="{{ $role->id }}">{{ $role->name }}</option>
			        		@endforeach
						</select>
			      	</div>
		    	</div>

		    	<div class="form-group">
			    	{!! Form::label('area', 'Area ', ['class' => 'col-md-4 control-label']) !!}
			      	<div class="col-md-4">
			        	<select name='area_id' class='selectpicker area-reg' data-live-search="true" id="content">
			        		<option value="">Choose area</option>
		        			@foreach($areas as $area)
			        		<option value="{{ $area->id }}">{{ $area->name }}</option>
			        		@endforeach
						</select>
			      	</div>
		    	</div>

				<div class="form-group">
					<label class="col-md-4 control-label">Username</label>
					<div class="col-sm-3">
						<input type="text" class="form-control" name="username" required value="{{ old('username') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">Name</label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="name" required value="{{ old('name') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">E-Mail Address</label>
					<div class="col-md-3">
						<input type="email" class="form-control" name="email" required value="{{ old('email') }}">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">Password</label>
					<div class="col-md-3">
						<input type="password" class="form-control" required name="password">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">Confirm Password</label>
					<div class="col-md-3">
						<input type="password" class="form-control" required name="password_confirmation">
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary">
							Register
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection