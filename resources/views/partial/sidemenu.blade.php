<ul class="nav" id="side-menu">
	<li>
		<a href="/home"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
	</li>
	<li>
		<a href="#collapse1" data-toggle="collapse" data-parent="#side-menu">
			<i class="glyphicon glyphicon-th-large"></i> 
			Master
			<i class="fa fa-caret-down arrow"></i>
		</a>
		<ul class="nav nav-second-level collapse" id="collapse1">
			<li>
				<a href="/brand">Brand</a>
			</li>
			<li>
				<a href="/product">Product</a>
			</li>
			<li>
				<a href="/product_taste">Product taste</a>
			</li>
			<li>
				<a href="/distributor">Distributor</a>
			</li>
			<li>
				<a href="/area">Area</a>
			</li>
			<li>
				<a href="/depo">Depo</a>
			</li>
			<li>
				@if(\Helper::OutletData()->isEmpty())
					<a href="/outlet/create">Outlet</a>
				@else
					<a href="/outlet">Outlet</a>
				@endif
			</li>
			<li>
				<a href="/taste">Taste varians</a>
			</li>
		</ul>
	</li>
	<li>
		<a href="/sellin"><i class="glyphicon glyphicon-arrow-right"></i> Sell In</a>
	</li>
	<li>
		<a href="/sellout/create"><i class="glyphicon glyphicon-arrow-left"></i> Sell Out</a>
	</li>
	<li>
		<a href="/stock"><i class="fa fa-bars"></i> Stock</a>
	</li>
	<li>
		<a href="/target/import"><i class="glyphicon glyphicon-screenshot"></i> Sales Target</a>
	</li>
	<li>
		<a href="#collapse2" data-toggle="collapse" data-parent="#side-menu">
			<i class="fa fa-bar-chart"></i> 
			Report
			<i class="fa fa-caret-down arrow"></i>
		</a>
			<ul class="nav nav-second-level collapse" id="collapse2">
				<li>
					<a href="/report/sellin">Sell In</a>
				</li>
				<li>
					<a href="/report/sellout">Sell Out</a>
				</li>
				<!-- <li>
					<a href="#">Stock</a>
				</li> -->
				<li>
					<a href="/report/trend">Trend Sell Out</a>
				</li>
			</ul>
	</li>
</ul>