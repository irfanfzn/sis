@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Depo</li>
		</ol>
		<div class="page-header">
			<h2>Depo</h2>
		</div>	
		<!-- <ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="#">TAB 1</a></li>
		  	<li role="presentation"><a href="#">TAB 2</a></li>
		</ul> -->


		{!! Form::open(['url' => 'depo/search', 'class' => 'form-inline form-custom', 'id' => 'search-form']) !!}
			<div class="form-group">
				<input type="hidden" name="field_url" value="/depo/field">
				{!! Form::label('field', 'Field : ') !!}
				<select name='field' class='selectpicker' data-live-search="true" id="field">
					<option value="">Choose field</option>
					<option value="areas.name">Area</option>
					<option value="depos.name">Name</option>
				</select>
			</div>
			<div class="form-group">
				{!! Form::label('parameter', 'Parameter : ') !!}
				<select name='parameter' class='selectpicker' data-live-search="true" id="content">
					<option value="">Parameter</option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Search</button>
			</div>
		{!! Form::close() !!}
		
		<div class="table-responsive" id="list_page" style="display: block;">
			<table class="table table-striped table-hover table-bordered ">
			  	<thead>
				    <tr>
				      <th>id</th>
				      <th>Area</th>
				      <th>Name</th>
				      <th>Cerated at</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  	<!-- FOREACH -->
			  	@foreach($data as $row)
				    <tr>
				      <td>{!! $row->id !!}</td>
				      <td>{!! $row->area_name !!}</td>
				      <td>{!! $row->depo_name !!}</td>
				      <td>{!! $row->created_at !!}</td>
				    </tr>
				@endforeach
				<!-- END FOREACH -->
			  </tbody>
			</table>
			{!! $data->render() !!}
			<!-- PAGING 	 -->
		</div>
	</div>
</div>

@endsection