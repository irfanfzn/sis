@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Sell In</li>
		</ol>
		<div class="page-header">
			<h2>Sell In</h2>
		</div>	
		<!-- <ul class="nav nav-tabs">
		  	<li role="presentation"><a href="#">TAB 1</a></li>
		  	<li role="presentation" class="active"><a href="#">TAB 2</a></li>
		</ul> -->
		<div id="create-page" class="well bs-component">
			{!! Form::open(['class' => 'form-horizontal','url' => 'sellin/create', 'files' => true, 'id' => 'uploadForm']) !!}
				<fieldset>
					<div class="form-group">
				      	{!! Form::label('excel', 'Excel file : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	{!! Form::file('excel', null, ['id' => 'excel']) !!}
				      	</div>
				    </div>
				    <div class="form-group">
				    	<div class="col-md-9 col-md-offset-3">
				    		{!! Form::submit('Import', ['class' => 'btn btn-primary', 'id' => 'outlet-import']) !!}
				    	</div>
				    </div>

				</fieldset>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="loading-progress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
	        <div class="modal-header">
		        <h4 class="modal-title" style="text-align:center">Importing data...</h4>
	      	</div>
      		<div class="modal-body">
				<div id="import-file">
      				<p class="col-md-offset-5">Importing file 
      				</p>
	      			<div class="progress progress-striped active">
						<div class="progress-bar" style="width: 100%"></div>
					</div>
				</div>

				<div id="error-message">
					<!-- AJAX ERROR APPEND HERE -->
				</div>

				<div id='success-message'>
					<!-- AJAX SUCCESS APPEND HERE -->
				</div>
      		</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary save-change">Save changes</button>
		    </div>
	    </div>
  	</div>
</div>
@endsection