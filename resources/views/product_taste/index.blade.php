@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Product</li>
		</ol>
		<div class="page-header">
			<h2>Product taste</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="#">Product taste list</a></li>
		  	<li role="presentation"><a href="/product_taste/create">Add product taste</a></li>
		  	<li role="presentation"><a href="/product_taste/import">Import product taste</a></li>
		</ul>

		{!! Form::open(['url' => 'product_taste/search', 'class' => 'form-inline form-custom', 'id' => 'search-form']) !!}
			<div class="form-group">
				<input type="hidden" name="field_url" value="/product_taste/field">
				{!! Form::label('field', 'Field : ') !!}
				<select name='field' class='selectpicker' data-live-search="true" id="field">
					<option value="">Choose field</option>
					<option value="products.name">Product name</option>
					<option value="outlet_types.name">Outlet type name</option>
					<option value="product_tastes.code">code</option>
					<option value="product_tastes.name">name</option>
					<option value="product_tastes.pcs">pcs</option>
				</select>
			</div>
			<div class="form-group">
				{!! Form::label('parameter', 'Parameter : ') !!}
				<select name='parameter' class='selectpicker' data-live-search="true" id="content">
					<option value="">Parameter</option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Search</button>
			</div>
		{!! Form::close() !!}
		
		<div class="table-responsive" id="list_page" style="display: block;">
			<table class="table table-striped table-hover table-bordered ">
			  	<thead>
				    <tr>
				      <th>id</th>
				      <th>Product</th>
				      <th>Outlet type</th>
				      <th>Code</th>
				      <th>Name</th>
				      <th>Pcs</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  	@foreach($product_tastes as $produc_taste)
				    <tr>
				      <td>{{ $produc_taste->id }}</td>
				      <td>{{ $produc_taste->product->name }}</td>
				      <td>{{ $produc_taste->outletType->name }}</td>
				      <td>{{ $produc_taste->code }}</td>
				      <td>{{ $produc_taste->name }}</td>
				      <td>{{ $produc_taste->pcs }}</td>
				    </tr>
				@endforeach
			  </tbody>
			</table>
		{!! $product_tastes->render() !!}	
		</div>
	</div>
</div>

@endsection