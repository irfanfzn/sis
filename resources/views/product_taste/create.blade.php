@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Product</li>
		</ol>
		<div class="page-header">
			<h2>Product taste</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation"><a href="/product_taste">Product taste list</a></li>
		  	<li role="presentation" class="active"><a href="#">Add product taste</a></li>
		  	<li role="presentation"><a href="/product_taste/import">Import product taste</a></li>
		</ul>
		<div id="create-page" class="well bs-component">
			{!! Session::get('message') ? \Helper::ShowMessage(Session::get('message'), 1) : '' !!}
			
			{!! Form::open(['class' => 'form-horizontal', 'files' => true, 'id' => 'createForm']) !!}
				<fieldset>

				    <div class="form-group">
				    	{!! Form::label('brand_id', 'Outlet type : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	<select name='outlet_type_id' class='selectpicker' data-live-search="true" id="content">
				        		<option value="">Choose outlet type</option>
			        		@foreach($outlet_types as $outlet_type)
				        		<option value="{{ $outlet_type->id }}">{{ $outlet_type->name }}</option>
				        	@endforeach
							</select>
				      	</div>
				    </div>

					<div class="form-group">
				      	{!! Form::label('brand_id', 'Product : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	<select name='product_id' class='selectpicker' data-live-search="true" id="content">
				        		<option value="">Choose product</option>
				        	@foreach($products as $product)
				        		<option value="{{ $product->id }}">{{ $product->name }}</option>
				        	@endforeach
							</select>
				      	</div>
				    </div>

				     <div class="form-group">
				    	{!! Form::label('brand_id', 'Taste : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	<select name='taste_id' class='selectpicker' data-live-search="true" id="content">
				        		<option value="">Choose taste</option>
			        		@foreach($tastes as $taste)
				        		<option value="{{ $taste->id }}">{{ $taste->name }}</option>
				        	@endforeach
							</select>
				      	</div>
				    </div>

				   <!--  <div class="form-group">
				      	{!! Form::label('code', 'Code : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-sm-2">
				        	{!! Form::text('code', null, ['id' => 'product-code', 'class' => 'form-control', 'required' => 'required']) !!}
				      	</div>
				    </div> -->

				    <div class="form-group">
				      	{!! Form::label('name', 'Product name : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	{!! Form::text('name', null, ['id' => 'product-name', 'class' => 'form-control', 'required' => 'required']) !!}
				      	</div>
				    </div>

				    <div class="form-group">
				      	{!! Form::label('name', 'Pcs : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	{!! Form::text('pcs', null, ['id' => 'product-pcs', 'class' => 'form-control', 'required' => 'required']) !!}
				      	</div>
				    </div>

				    <div class="form-group">
				    	<div class="col-md-9 col-md-offset-3">
				    		{!! Form::submit('Submit', ['class' => 'btn btn-primary', 'id' => 'outlet-import']) !!}
				    	</div>
				    </div>

				</fieldset>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection