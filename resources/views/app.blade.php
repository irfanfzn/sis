<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SIS</title>

	<!-- <link href="{{-- asset('/theme/css/app.css') --}}" rel="stylesheet"> -->
	<link href="{{ asset('/theme/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/theme/css/style.css') }}" rel="stylesheet">
	<link href="{{ asset('/theme/css/custom.css') }}" rel="stylesheet">
	<link href="{{ asset('/theme/css/bootstrap-select.css') }}" rel="stylesheet">
	<link href="{{ asset('/theme/css/select2.css') }}" rel="stylesheet">
	<link href="{{ asset('/theme/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
	<!-- <link href="{{ asset('/theme/css/bootstrap-theme.min.css') }}" rel="stylesheet"> -->

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/') }}">S I S</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<!-- <li><a href="#">menu 1</a></li> -->
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Register</a></li>
					@else
						@if(\Helper::getUserRole() == 'Administrator')
						<li><a href="/user">Manage User</a></li>
						@endif
						
						<li class="dropdown">

							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/cleardata') }}">Clear Data</a></li>
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	<nav class="navbar-static-side navbar-default" role="navigation">
		<div class="sidebar-collapse">
				@include('partial.sidemenu')
		</div>
	</nav>

	<div id="page-wrapper">

		@yield('content')

		<hr>
		
		<footer>
			<p class="text-center">2015 © Irfan Fauzan.</p>
		</footer>

	</div>

	<!-- Scripts -->
	<script src="{{ asset('/theme/js/jquery-2.1.3.min/index.js') }}"></script>
	<script src="{{ asset('/theme/js/jquery.form.min.js') }}"></script>
	<script src="{{ asset('/theme/js/bootstrap-select.js') }}"></script>
	<script src="{{ asset('/theme/js/bootstrap-select.min.js') }}"></script>
	<script src="{{ asset('/theme/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('/theme/js/select2.js') }}"></script>
	<script src="{{ asset('/theme/js/canvasjs.min.js') }}"></script>
	<!-- Custom js -->
	<script src="{{ asset('/theme/js/script.js') }}"></script>
</body>
</html>
