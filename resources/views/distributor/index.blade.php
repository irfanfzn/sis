@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Distributor</li>
		</ol>
		<div class="page-header">
			<h2>Distributor</h2>
		</div>	
		<!-- <ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="#">Distributor list</a></li>
		  	<li role="presentation"><a href="#">TAB 2</a></li>
		</ul> -->


		{!! Form::open(['url' => 'distributor/search', 'class' => 'form-inline form-custom', 'id' => 'search-form']) !!}
			<div class="form-group">
				<input type="hidden" name="field_url" value="/distributor/field">
				{!! Form::label('field', 'Field : ') !!}
				<select name='field' class='selectpicker' data-live-search="true" id="field">
					<option value="">Choose field</option>
					<option value="name">Name</option>
				</select>
			</div>
			<div class="form-group">
				{!! Form::label('parameter', 'Parameter : ') !!}
				<select name='parameter' class='selectpicker' data-live-search="true" id="content">
					<option value="">Parameter</option>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Search</button>
			</div>
		{!! Form::close() !!}
		
		<div class="table-responsive" id="list_page" style="display: block;">
			<table class="table table-striped table-hover table-bordered ">
			  	<thead>
				    <tr>
				      <th>id</th>
				      <th>Name</th>
				      <th>Created at</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  	<!-- FOREACH -->
			  	@foreach($data as $row)
				    <tr>
				      <td>{!! $row->id !!}</td>
				      <td>{!! $row->name !!}</td>
				      <td>{!! $row->created_at !!}</td>
				    </tr>
				@endforeach
				<!-- END FOREACH -->
			  </tbody>
			</table>
			<!-- PAGING 	 -->
			{!! $data->render() !!}	
		</div>
	</div>
</div>

@endsection