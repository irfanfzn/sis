@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<!-- BREADCRUMB -->
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Stock</li>
		</ol>
		<div class="page-header">
			<h2>Stock</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation"><a href="/stock">Stock list</a></li>
		  	<li role="presentation"><a href="/stock/create">Adjustment import</a></li>
		  	<li role="presentation" class="active"><a href="#">Create adjustment</a></li>
		</ul>
		<!-- END -->
		<div id="create-page" class="well bs-component">
			{!! Session::get('message') ? \Helper::ShowMessage(Session::get('message'), 1) : '' !!}

			{!! \Helper::ErrorMessage($errors)!!}
			
			{!! Form::open(['class' => 'form-horizontal', 'files' => true, 'id' => 'createForm']) !!}
				<fieldset>
					<div class="form-group">
				      	{!! Form::label('brand_id', 'Depo : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	<select name='depo_id' class='selectpicker' data-live-search="true" id="content">
				        		<option value="">Choose depo</option>
				        		@foreach($depos as $depo)
			        			<option value="{{ $depo->id }}">{{ $depo->name }}</option>
				        		@endforeach
							</select>
				      	</div>
				    </div>

				    <div class="form-group">
				      	{!! Form::label('brand_id', 'Product taste : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	<select name='product_taste_id' class='selectpicker' data-live-search="true" id="content">
				        		<option value="">Choose product taste</option>
				        		@foreach($product_tastes as $product_taste)
			        			<option value="{{ $product_taste->id }}">{{ $product_taste->outletType->name }} - {{ $product_taste->name }}</option>
				        		@endforeach
							</select>
				      	</div>
				    </div>

				    <div class="form-group">
				      	{!! Form::label('Stock', 'Stock : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-sm-2">
				        	{!! Form::text('stock', null, ['id' => 'product-code', 'class' => 'form-control', 'required' => 'required']) !!}
				      	</div>
				    </div>

				    <div class="form-group">
				      	{!! Form::label('up1', 'UP1 : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-sm-2">
				        	{!! Form::text('up1', null, ['id' => 'product-name', 'class' => 'form-control', 'required' => 'required']) !!}
				      	</div>
				    </div>

				    <div class="form-group">
				      	{!! Form::label('up1', 'UP2 : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-sm-2">
				        	{!! Form::text('up2', null, ['id' => 'product-name', 'class' => 'form-control', 'required' => 'required']) !!}
				      	</div>
				    </div>

				    <div class="form-group">
				      	{!! Form::label('up3', 'UP3 : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-sm-2">
				        	{!! Form::text('up3', null, ['id' => 'product-name', 'class' => 'form-control', 'required' => 'required']) !!}
				      	</div>
				    </div>

				    <div class="form-group">
				    	<div class="col-md-9 col-md-offset-3">
				    		{!! Form::submit('Submit', ['class' => 'btn btn-primary', 'id' => 'outlet-import']) !!}
				    	</div>
				    </div>

				</fieldset>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endsection