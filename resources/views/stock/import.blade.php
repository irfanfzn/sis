@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Stock</li>
		</ol>
		<div class="page-header">
			<h2>Stock</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation"><a href="/stock">Stock list</a></li>
		  	<li role="presentation" class="active"><a href="#">Adjustment import</a></li>
		  	<li role="presentation"><a href="/stock/form">Create adjustment</a></li>
		</ul>
		<div id="create-page" class="well bs-component">
			{!! \Helper::ErrorMessage($errors) !!}
			
			{!! Form::open(['class' => 'form-horizontal', 'files' => true, 'id' => 'uploadForm']) !!}
				<fieldset>
					<div class="form-group">
					{!! Form::label('parameter', 'Stock method : ', ['class' => 'control-label col-md-3']) !!}
						<div class="col-md-4">
							<select name='method' class='selectpicker' data-live-search="true" id="content">
				        		<option value="">Choose method</option>
			        			<option value="first_stock">First stock</option>
			        			<option value="adj">Adjustment stock</option>
			        			<option value="ho">HO return</option>
							</select>
						</div>
						<!-- <button type="submit" class="btn btn-primary">Search</button> -->
					</div>

					<div class="form-group">
				      	{!! Form::label('brand_id', 'Depo : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	<select name='depo_id' class='selectpicker' data-live-search="true" id="content">
				        		<option value="">Choose Depo</option>
				        		@foreach($depos as $depo)
				        			<option value="{{ $depo->id }}">{{ $depo->name }}</option>
				        		@endforeach
							</select>
				      	</div>
				    </div>
					<div class="form-group">
				      	{!! Form::label('excel', 'Excel file : ', ['class' => 'control-label col-md-3']) !!}
				      	<div class="col-md-4">
				        	{!! Form::file('excel', null, ['id' => 'excel']) !!}
				      	</div>
				    </div>
				    <div class="form-group">
				    	<div class="col-md-9 col-md-offset-3">
				    		{!! Form::submit('Import', ['class' => 'btn btn-primary', 'id' => 'outlet-import']) !!}
				    	</div>
				    </div>

				</fieldset>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="loading-progress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
	        <div class="modal-header">
		        <h4 class="modal-title" style="text-align:center">Importing data...</h4>
	      	</div>
      		<div class="modal-body">
				<div id="import-file">
      				<p class="col-md-offset-5">Importing file 
      				</p>
	      			<div class="progress progress-striped active">
						<div class="progress-bar" style="width: 100%"></div>
					</div>
				</div>

				<div id="error-message">
					<!-- AJAX ERROR APPEND HERE -->
				</div>

				<div id='success-message'>
					<!-- AJAX SUCCESS APPEND HERE -->
				</div>
      		</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary save-change">Save changes</button>
		    </div>
	    </div>
  	</div>
</div>
@endsection