@extends('app')

@section('content')

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
		  	<li><a href="#">Dashboard</a></li>
		  	<li class="active">Stock</li>
		</ol>
		<div class="page-header">
			<h2>Stock</h2>
		</div>	
		<ul class="nav nav-tabs">
		  	<li role="presentation" class="active"><a href="#">Stock list</a></li>
		  	<li role="presentation"><a href="/stock/create">Adjustment import</a></li>
		  	<li role="presentation"><a href="/stock/form">Create adjustment</a></li>
		</ul>

		{!! Form::open(['url' => 'stock/search', 'class' => 'form-horizontal form-custom', 'id' => 'search-form']) !!}
		<fieldset>
			<div class="col-md-4">
				<div class="form-group">
				{!! Form::label('parameter', 'Depo : ') !!}<br>
					<select class="lists" multiple="multiple" name="depos[]">
						@foreach($depos as $depo)
					  	<option value="{{ $depo->id }}" 
					  	{{ \Helper::GetInput(Input::get('depos'), $depo->id) }}
					  	>{{ $depo->name }}</option>
					  	@endforeach
					</select>
					<!-- <button type="submit" class="btn btn-primary">Search</button> -->
				</div>

				<div class="form-group">
					{!! Form::label('field', 'Area : ') !!}<br>
					<select class="lists" multiple="multiple" name="areas[]">
						@foreach($areas as $area)
							<option value="{!! $area->id !!}"
							{{ \Helper::GetInput(Input::get('areas'), $area->id) }}
							>{!! $area->name !!}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-primary">Search</button>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					{!! Form::label('field', 'Product : ') !!}<br>
					<select class="lists" multiple="multiple" name="products[]">
						@foreach($products as $product)
							<option value="{!! $product->id !!}" 
							{{ \Helper::GetInput(Input::get('products'), $product->id) }}
							>{!! $product->name !!}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					{!! Form::label('field', 'Outlet type : ') !!}<br>
					<select class="lists" multiple="multiple" name="outlet_types[]">
						@foreach($outlet_types as $outlet_type)
							<option value="{!! $outlet_type->id !!}" 
								{{ \Helper::GetInput(Input::get('outlet_types'), $outlet_type->id) }}
								>
								{!! $outlet_type->name !!}
							</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					{!! Form::label('field', 'Product taste : ') !!}<br>
					<select class="lists" multiple="multiple" name="product_tastes[]">
						@foreach($product_tastes as $product_taste)
							<option value="{!! $product_taste->name !!}"
							{{ \Helper::GetInput(Input::get('product_tastes'), $product_taste->name) }}
							>{!! $product_taste->name !!}</option>
						@endforeach
					</select>
				</div>
			</div>
		</fieldset>
		{!! Form::close() !!}
		
		<div class="table-responsive" id="list_page" style="display: block;">
			<table class="table table-striped table-hover table-bordered ">
			  	<thead>
				    <tr>
				      <!-- <th>Depo</th> -->
				      <th>Product taste</th>
				      <th>Outlet type</th>
				      <!-- <th>Expired date</th> -->
				      <th>Stock</th>
				      <th>UP-1</th>
				      <th>UP-2</th>
				      <th>UP-3</th>
				      <th>Average</th>
				      <th>SCM</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  	<!-- FOREACH -->
			  	@foreach($stocks as $stock)
			  		<?php $scm = !empty($stock->stock_avg) ? $stock->stock / $stock->stock_avg : 0; ?>
				    <tr>
				      <!-- <td>{!! $stock->depo !!}</td> -->
				      <td>{!! $stock->variant !!}</td>
				      <td>{!! $stock->outlet_type !!}</td>
				      <!-- <td>{!! !empty($stock->exp_date) ? \Helper::DateFormat($stock->exp_date) : "" !!}</td> -->
				      <td>{!! number_format($stock->stock, 2) !!}</td>
				      <!-- <td>{!! $stock->count_stock !!}</td> -->
				      <td>{!! number_format($stock->up1, 2) !!}</td>
				      <td>{!! number_format($stock->up2, 2) !!}</td>
				      <td>{!! number_format($stock->up3, 2) !!}</td>
				      <td>{!! number_format($stock->stock_avg, 2) !!}</td>
				      <td>{!! number_format($scm, 2) !!}</td>
				    </tr>
				@endforeach
				<!-- END FOREACH -->
			  </tbody>
			</table>
			<!-- PAGING 	 -->
		</div>
	</div>
</div>

@endsection