<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellInsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sell_ins', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('distributor_id');
			$table->integer('depo_id');
			$table->integer('outlet_type_id');
			$table->integer('product_taste_id');
			$table->dateTime('sellin_date');
			$table->string('order_id');
			$table->double('qty_ctn');
			$table->string('tax');
			$table->string('status');
			$table->string('unit');
			$table->integer('amount');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sell_ins');
	}

}
