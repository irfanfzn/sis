<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTastesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_tastes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('product_id');
			$table->integer('outlet_type_id');
			$table->string('code');
			$table->string('name');
			$table->Integer('pcs');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_tastes');
	}

}
