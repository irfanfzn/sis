<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProductIdNameSellOutTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sell_outs', function(Blueprint $table)
		{
			$table->renameColumn('product_id', 'product_taste_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sell_outs', function(Blueprint $table)
		{
			$table->renameColumn('product_taste_id', 'product_id');
		});
	}

}
