<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOutletIdToCellOutsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sell_outs', function(Blueprint $table)
		{
			$table->integer('outlet_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sell_outs', function(Blueprint $table)
		{
			$table->dropColumn('outlet_id');
		});
	}

}
