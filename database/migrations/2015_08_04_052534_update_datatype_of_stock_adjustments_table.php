<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDatatypeOfStockAdjustmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::update("ALTER TABLE stock_adjustments MODIFY up2 DOUBLE");
		DB::update("ALTER TABLE stock_adjustments MODIFY up3 DOUBLE");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::update("ALTER TABLE stock_adjustments MODIFY up2 INTEGER(255)");
		DB::update("ALTER TABLE stock_adjustments MODIFY up3 INTEGER(255)");
	}

}
