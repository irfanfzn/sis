<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellOutsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sell_outs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('product_id');
			$table->integer('outlet_type_id');
			$table->integer('depo_id');
			$table->integer('sale_id');
			$table->integer('sales_promotion_id');
			$table->integer('area_id');
			$table->dateTime('sell_date');
			$table->integer('qty_ctn');
			$table->integer('qty_pcs');
			$table->double('qty_converted_pcs');
			$table->double('qty_total');
			$table->integer('return_ctn');
			$table->integer('return_pcs');
			$table->double('return_converted_pcs');
			$table->double('return_total');
			$table->double('omzet');
			$table->double('gross');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sell_outs');
	}

}
