<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBestEstimateTargetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('best_estimate_targets', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('product_id');
			$table->integer('area_id');
			$table->integer('outlet_type_id');
			$table->integer('outlet_group_id');
			$table->integer('month_num');
			$table->string('target_month');
			$table->integer('target_year');
			$table->double('target');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('best_estimate_targets');
	}

}
