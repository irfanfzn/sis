<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeGrossToNetCellOutsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sell_outs', function($table)
		{
			$table->renameColumn('gross', 'net');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sell_outs', function($table)
		{
			$table->renameColumn('net', 'gross');
		});
	}

}
