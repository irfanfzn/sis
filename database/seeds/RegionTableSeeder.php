<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RegionTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('regions')->truncate();

		$regions = [
			[
				'parent_region_id' => 2,
				'name' => 'SUMBAGUT'
			],
			[
				'parent_region_id' => 2,
				'name' => 'SUMBAGSEL'
			],
			[
				'parent_region_id' => 2,
				'name' => 'IBT'
			],
			[
				'parent_region_id' => 1,
				'name' => 'JAWA'
			]
		];

		foreach($regions as $region)
		{
			App\Region::create($region);
		}
	}

}
