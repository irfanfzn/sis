<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BrandTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('brands')->truncate();

		$brands =[
			[
				'name' => 'Gizo',
				'status' => 'Eksis'
			],
			[
				'name' => 'Sogi',
				'status' => 'Pasif'	
			]
		];

		foreach($brands as $brand)
		{
			App\Brand::create($brand);
		}
	}

}
