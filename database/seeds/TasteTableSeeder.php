<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TasteTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('tastes')->truncate();

		$tastes = [
			[
				'code' => 'A',
				'name' => 'Coklat'
			],
			[
				'code' => 'B',
				'name' => 'Strawberry'
			],
			[
				'code' => 'C',	
				'name' => 'Orange'
			],
			[
				'code' => 'D',	
				'name' => 'Vanilla'
			],
			[
				'code' => 'E',	
				'name' => 'Tutty Fruity'
			],
			[
				'code' => 'F',	
				'name' => 'Banana'
			],
			[
				'code' => 'G',	
				'name' => 'Strawberry-Orange'
			],
			[
				'code' => 'H',	
				'name' => 'Strawberry-Vanilla'
			],
			[
				'code' => 'I',	
				'name' => 'Milk'
			],
			[
				'code' => 'L',	
				'name' => 'Blueberry'
			],
			[
				'code' => 'X',	
				'name' => 'Mix'
			]
		];

		foreach($tastes as $taste)
		{
			App\Taste::create($taste);
		}
	}

}
