<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ProductTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('products')->truncate();

		$products = [
				[
	        		'brand_id' => '1',
	        		'code' => 'CK',
	        		'name' => 'Chokoku'
	        	],
	        	[
	        		'brand_id' => '1',
	        		'code' => 'GE',
	        		'name' => 'Eskimu'
	        	],
	        	[
	        		'brand_id' => '1',
	        		'code' => 'GW',
	        		'name' => 'Wow'
	        	],
	        	[
	        		'brand_id' => '1',
	        		'code' => 'CN',
	        		'name' => 'Cocone'
	        	],
	        	[
	        		'brand_id' => '2',
	        		'code' => 'SB',
	        		'name' => 'Brolly'
	        	],
	        	[
	        		'brand_id' => '2',
	        		'code' => 'SR',
	        		'name' => 'Broo'
	        	],
	        	[
	        		'brand_id' => '2',
	        		'code' => 'CO',
	        		'name' => 'Conie'
	        	],
	        	[
	        		'brand_id' => '2',
	        		'code' => 'PO',
	        		'name' => 'Pio'
	        	],
	        	[
	        		'brand_id' => '2',
	        		'code' => 'ZC',
	        		'name' => 'Zupercone'
	        	]
        	];

        foreach($products as $product)
        {
        	App\Product::create($product);	
        }
	}

}
