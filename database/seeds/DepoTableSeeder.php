<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DepoTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('depos')->truncate();

		$depos = [
			[
				'area_id' => '1',
				'name' => 'Cirebon'
			],
			[
				'area_id' => '1',
				'name' => 'Bandung'
			],
			[
				'area_id' => '2',
				'name' => 'Pluit'
			],
			[
				'area_id' => '2',
				'name' => 'Serang'
			],
			[
				'area_id' => '2',
				'name' => 'Tangerang'
			],
			[
				'area_id' => '2',
				'name' => 'Bekasi'
			],
			[
				'area_id' => '2',
				'name' => 'Malaka'
			],
			[
				'area_id' => '3',
				'name' => 'Malang'
			],
			[
				'area_id' => '3',
				'name' => 'Surabaya'
			],
			[
				'area_id' => '3',
				'name' => 'Jember'
			],
			[
				'area_id' => '3',
				'name' => 'Kediri'
			],
			[
				'area_id' => '4',
				'name' => 'Semarang'
			],
			[
				'area_id' => '4',
				'name' => 'Yogyakarta'
			],
			[
				'area_id' => '5',
				'name' => 'Bali'
			],
			[
				'area_id' => '6',
				'name' => 'Pekanbaru'
			],
			[
				'area_id' => '7',
				'name' => 'Medan'
			],
			[
				'area_id' => '8',
				'name' => 'Palembang'
			],
			[
				'area_id' => '9',
				'name' => 'Lampung'
			],
			[
				'area_id' => '10',
				'name' => 'Makasar'
			],
			[
				'area_id' => '11',
				'name' => 'Samarinda'
			],
			[
				'area_id' => '12',
				'name' => 'Banjarmasin'
			],
			[
				'area_id' => '13',
				'name' => 'Pontianak'
			],
			[
				'area_id' => '14',
				'name' => 'Balikpapan'
			],
		];

		foreach($depos as $depo)
		{
			App\Depo::create($depo);
		}
	}

}
