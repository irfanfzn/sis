<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ParentRegionTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('parent_regions')->truncate();

		$parent_regions = [
			[
				'name' => 'Pulau Jawa',
				'short_name' => 'PJ'
			],
			[
				'name' => 'Luar Pulau Jawa',
				'short_name' => 'LPJ'
			]
		];

		foreach($parent_regions as $parent_region)
		{
			App\ParentRegion::create($parent_region);
		}
	}

}
