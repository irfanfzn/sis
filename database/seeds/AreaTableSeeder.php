<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AreaTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('areas')->truncate();

		$areas = [
			[
				'region_id' => 4,
				'name' => 'Jabar'
			],
			[
				'region_id' => 4,
				'name' => 'Jabodetabek'
			],
			[
				'region_id' => 4,
				'name' => 'Jatim'
			],
			[
				'region_id' => 4,
				'name' => 'Jateng'
			],
			[
				'region_id' => 3,
				'name' => 'Bali'
			],
			[
				'region_id' => 1,
				'name' => 'Pekanbaru'
			],
			[
				'region_id' => 1,
				'name' => 'Medan'
			],
			[
				'region_id' => 2,
				'name' => 'Palembang'
			],
			[
				'region_id' => 2,
				'name' => 'Lampung'
			],
			[
				'region_id' => 3,
				'name' => 'Makasar'
			],
			[
				'region_id' => 3,
				'name' => 'Samarinda'
			],
			[
				'region_id' => 3,
				'name' => 'Banjarmasin'
			],
			[
				'region_id' => 3,
				'name' => 'Pontianak'
			],
			[
				'region_id' => 3,
				'name' => 'Balikpapan'
			],
		];

		foreach($areas as $area)
		{
			App\Area::create($area);
		}
	}

}
