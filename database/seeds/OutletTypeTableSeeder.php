<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class OutletTypeTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('outlet_types')->truncate();

		$outlet_types = [
			[
				'name' => 'IDM'
			],
			[
				'name' => 'SAT'
			],
			[
				'name' => 'LOCAL'
			]
		]; 

		foreach($outlet_types as $outlet_type)
		{
			App\OutletType::create($outlet_type);
		}
	}

}
