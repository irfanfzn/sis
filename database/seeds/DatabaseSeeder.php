<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('ProductTableSeeder');
		$this->call('BrandTableSeeder');
		$this->call('TasteTableSeeder');
		// $this->call('ProductTasteTableSeeder');
		$this->call('OutletTypeTableSeeder');
		$this->call('AreaTableSeeder');
		$this->call('DepoTableSeeder');
		$this->call('RegionTableSeeder');
		$this->call('ParentRegionTableSeeder');
		$this->call('UserRoleTableSeeder');
	}

}
