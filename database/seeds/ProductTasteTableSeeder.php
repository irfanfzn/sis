<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ProductTasteTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('product_tastes')->truncate();

		$product_tastes = [
		[
			'product_id' => 1, 
			'outlet_type_id' => 1, 
			'code' => 'CKB047LPJ', 
			'name' => 'CHOKOKU STRAWBERRY', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 1, 
			'outlet_type_id' => 1, 
			'code' => 'CKB047PJ', 
			'name' => 'CHOKOKU STRAWBERRY', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 1, 
			'outlet_type_id' => 3, 
			'code' => 'CKB048LPJ', 
			'name' => 'CHOKOKU STRAWBERRY', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 1, 
			'outlet_type_id' => 3, 
			'code' => 'CKB048PJ', 
			'name' => 'CHOKOKU STRAWBERRY', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 1, 
			'outlet_type_id' => 3, 
			'code' => 'CKC048LPJ', 
			'name' => 'CHOKOKU ORANGE', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 1, 
			'outlet_type_id' => 3, 
			'code' => 'CKC048PJ', 
			'name' => 'CHOKOKU ORANGE', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 1, 
			'outlet_type_id' => 3, 
			'code' => 'CKD046LPJ', 
			'name' => 'CHOKOKU VANILLA', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 1, 
			'outlet_type_id' => 3, 
			'code' => 'CKD046PJ', 
			'name' => 'CHOKOKU VANILLA', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 1, 
			'outlet_type_id' => 1, 
			'code' => 'CKE047LPJ', 
			'name' => 'CHOKOKU TUTTY FRUITTY', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 1, 
			'outlet_type_id' => 1, 
			'code' => 'CKE047PJ', 
			'name' => 'CHOKOKU TUTTY FRUITTY', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 1, 
			'outlet_type_id' => 3, 
			'code' => 'CKE048LPJ', 
			'name' => 'CHOKOKU TUTTY FRUITTY', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 1, 
			'outlet_type_id' => 3, 
			'code' => 'CKE048PJ', 
			'name' => 'CHOKOKU TUTTY FRUITTY', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 1, 
			'outlet_type_id' => 1, 
			'code' => 'CKL047LPJ', 
			'name' => 'CHOKOKU BLUEBERRY', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 1, 
			'outlet_type_id' => 1, 
			'code' => 'CKL047PJ', 
			'name' => 'CHOKOKU BLUEBERRY', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 1, 
			'outlet_type_id' => 3, 
			'code' => 'CKL048LPJ', 
			'name' => 'CHOKOKU BLUEBERRY', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 1, 
			'outlet_type_id' => 3, 
			'code' => 'CKL048PJ', 
			'name' => 'CHOKOKU BLUEBERRY', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 4, 
			'outlet_type_id' => 3, 
			'code' => 'CNA004LPJ', 
			'name' => 'COCONE COKLAT', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 4, 
			'outlet_type_id' => 3, 
			'code' => 'CNA004PJ', 
			'name' => 'COCONE COKLAT', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 2, 
			'outlet_type_id' => 1, 
			'code' => 'GEA022LPJ', 
			'name' => 'ESKIMU COKLAT', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 2, 
			'outlet_type_id' => 1, 
			'code' => 'GEA022PJ', 
			'name' => 'ESKIMU COKLAT', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 2, 
			'outlet_type_id' => 3, 
			'code' => 'GEA023LPJ', 
			'name' => 'ESKIMU COKLAT', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 2, 
			'outlet_type_id' => 3, 
			'code' => 'GEA023PJ', 
			'name' => 'ESKIMU COKLAT', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 2, 
			'outlet_type_id' => 1, 
			'code' => 'GEB022LPJ', 
			'name' => 'ESKIMU STRAWBERRY', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 2, 
			'outlet_type_id' => 1, 
			'code' => 'GEB022PJ', 
			'name' => 'ESKIMU STRAWBERRY', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 2, 
			'outlet_type_id' => 3, 
			'code' => 'GEB023LPJ', 
			'name' => 'ESKIMU STRAWBERRY', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 2, 
			'outlet_type_id' => 3, 
			'code' => 'GEB023PJ', 
			'name' => 'ESKIMU STRAWBERRY', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 2, 
			'outlet_type_id' => 1, 
			'code' => 'GEL022LPJ', 
			'name' => 'ESKIMU BLUEBERRY', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 2, 
			'outlet_type_id' => 1, 
			'code' => 'GEL022PJ', 
			'name' => 'ESKIMU BLUEBERRY', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 2, 
			'outlet_type_id' => 3, 
			'code' => 'GEL023LPJ', 
			'name' => 'ESKIMU BLUEBERRY', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 2, 
			'outlet_type_id' => 3, 
			'code' => 'GEL023PJ', 
			'name' => 'ESKIMU BLUEBERRY', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 0, 
			'outlet_type_id' => 3, 
			'code' => 'GTI001LPJ', 
			'name' => 'TAMA', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 0, 
			'outlet_type_id' => 3, 
			'code' => 'GTI001PJ', 
			'name' => 'TAMA', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 3, 
			'outlet_type_id' => 1, 
			'code' => 'GWB021LPJ', 
			'name' => 'WOW STRAWBERRY', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 3, 
			'outlet_type_id' => 1, 
			'code' => 'GWB021PJ', 
			'name' => 'WOW STRAWBERRY', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 3, 
			'outlet_type_id' => 3, 
			'code' => 'GWB022LPJ', 
			'name' => 'WOW STRAWBERRY', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 3, 
			'outlet_type_id' => 3, 
			'code' => 'GWB022PJ', 
			'name' => 'WOW STRAWBERRY', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 3, 
			'outlet_type_id' => 1, 
			'code' => 'GWB023LPJ', 
			'name' => 'WOW STRAWBERRY', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 3, 
			'outlet_type_id' => 1, 
			'code' => 'GWB023PJ', 
			'name' => 'WOW STRAWBERRY', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			],
			[
			'product_id' => 3, 
			'outlet_type_id' => 3, 
			'code' => 'GWX001LPJ', 
			'name' => 'WOW MIX', 
			'area' => 'Luar Jabodetabek', 
			'area_prefix' => 'OJA' 
			],
			[
			'product_id' => 3, 
			'outlet_type_id' => 3, 
			'code' => 'GWX001PJ', 
			'name' => 'WOW MIX', 
			'area' => 'Jabodetabek', 
			'area_prefix' => 'JA' 
			]
		];

        foreach($product_tastes as $product_taste)
        {
        	App\ProductTaste::create($product_taste);	
        }
	}

}
