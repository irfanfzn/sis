<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserRoleTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('user_roles')->truncate();

		$user_roles =[
			[
				'name' => 'Administrator',
			],
			[
				'name' => 'Moderator',
			],
			[
				'name' => 'Area Handler',
			]
		];

		foreach($user_roles as $user_role)
		{
			App\UserRole::create($user_role);
		}
	}
}
