$(document).ready(function()
{	
	/*var file = '/uploads/log_file.txt';*/
	var token = $('input[name=_token');
	$(".progress").hide();
	var counter = 0;

	// $('#outlet-import').click(function()
	// {
		

		/*$('#loading-progress').modal(
		{
		    backdrop: 'static',
		    keyboard: false
		});*/

	// });

	$('#uploadForm').submit(function(e) {
	// $('#dis').submit(function(e) {
			
		if($('#excel').val()) {
			e.preventDefault();

			$('#loading-progress').modal(
			{
			    backdrop: 'static',
			    keyboard: false
			});

			$(this).ajaxSubmit({
				beforeSubmit: function() {
				  $(".progress-bar").show();
				},
				/*xhr: function () {
				 	var progressElem = $('#progressCounter');
			        var xhr = new window.XMLHttpRequest();

			        //Download progress
			        xhr.upload.addEventListener("progress", function (evt) {
			        	console.log(evt.total);
			            
			            if (evt.lengthComputable) {
			                var percentComplete = evt.loaded / evt.total;
			                progressElem.html(Math.round(percentComplete * 100) + "%");
			            }
			        }, false);
			        return xhr;
			    },*/
				beforeSend: function (e){
					$(".progress").show();
				},
				success:function (data){
					$('.progress').hide();

					$('#error-message').html('');
					$('#success-message').html('');
					
					$.each(data, function(key, value)
					{
						var errorDiv = '<div class="alert alert-dismissible alert-warning">';
			            errorDiv += '<p><strong>WARNING!</strong> '+ value +' </p> ';
			            errorDiv += '</div>';	
			            $('#error-message').append(errorDiv).fadeIn('slow');	
					});

					var html = '<div class="alert alert-dismissible alert-success">';
					html += '<strong>Well done!</strong> Import is finished.';
					html += '</div>';

					$('#success-message').append(html).fadeIn('slow');

					$('#loading-progress .modal-body').animate({scrollTop: $("#loading-progress .modal-body div:last").offset().top + 1000},0);
				},
				error:function(data)
				{
					$('.progress').hide();
					
					$('#error-message').html('');
					$('#success-message').html('');

					var errorDiv = '<div class="alert alert-dismissible alert-danger">';
			            errorDiv += '<p><strong>ERROR!</strong> '+ data.responseText +' </p> ';
			            errorDiv += '</div>';	
			            $('#error-message').append(errorDiv).fadeIn('slow');
				},
				resetForm: true 
			}); 
			return false; 
		}
	});

	$('#field').change(function()
	{
		var field = $('#field').val();
		var form = $('#search-form');

		$.ajax({
			type: 'POST',
			url: '/outlet/field',
			dataType: 'json',
			data: form.serialize(),
			success: function(data)
			{
				$('#content').empty().append('<option value="">Parameter</option>');
				
				$.each(data, function(key, value)
				{
					$('#content').append('<option value="'+ value.content +'">'+ value.content +'</option>');
				});
				
				$('#content').selectpicker('refresh');
			}
		});
	});

	$('.area-reg').prop('disabled', true);

	$('.role-reg').change(function()
	{
		var role_val = $(this).val();
		
		if(role_val == 3)
		{
			$('.area-reg').prop('disabled', false);			
			console.log('this is 3');
		}
		else
		{
			$('.area-reg').prop('disabled', true);		
		}

		$('.area-reg').selectpicker('refresh');

	});

	$(".lists").select2();	
});

/*window.onload = function()
{
	chart();
}*/

/*--- Chart ---*/
/*function chart(data)
{
	var chart = new CanvasJS.Chart("chartContainer", {
		axisX: {
			labelFontSize: 10
		},

		axisY: {
			labelFontSize: 10
		},

		data: 
		[{        
			type: "line",
			dataPoints:
			[
				{ x: 10, y: 21 },
				{ x: 20, y: 25 },
				{ x: 30, y: 20 },
				{ x: 40, y: 25 },
				{ x: 50, y: 27 },
				{ x: 60, y: 28 },
				{ x: 70, y: 28 },
				{ x: 80, y: 24 },
				{ x: 90, y: 26 }
			],
		},
		{
			type: "line",
	        dataPoints: [
				{ x: 10, y: 31 },
				{ x: 20, y: 35 },
				{ x: 30, y: 30 },
				{ x: 40, y: 35 },
				{ x: 50, y: 35 },
				{ x: 60, y: 38 },
				{ x: 70, y: 38 },
				{ x: 80, y: 34 },
				{ x: 90, y: 44 }
	        ],
		}

		]
	});

	chart.render();
}*/